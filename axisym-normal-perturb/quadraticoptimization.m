% quadratic optimization
% hg May 5 2020
clear
close all
addpath(genpath('./ruowen_Bspline'))
addpath(genpath('./hai_adapt'))

% Form matrices
% Prescribe shape, form mobility matrix
p=10; N = 40*p; qtype = 'p'; qntype = 'G'; s0.p = p;
[stdt, ~] = gauss(p); stdbw = baryweights(stdt);

alpha = 1;
% alpha = (1+2.5^2)^.5;

% s0.Z = @(t) 1/alpha*sin(t) + 1i*cos(t);   % spherical shape
s0.Z = @(t) (alpha^(-1/3)*sin(t) + 1i*alpha^(2/3)*cos(t)); % ellipsoid with conserved volume

[s0,~] = quadrp(s0, N, qtype, qntype); 
s = half_quadr(s0); 
s.nx = -s.nx;

% load('caterpillarshape.mat');
% % load('almostsphereshape.mat');
% [s0,~] = quadrp(s0, N, qtype, qntype); 
% s = half_quadr(s0); 

% % compute volume
% rr = real(s.x);
% Vol = abs(trapz(imag(s.x),rr.^2)*pi);
% r_scaled = (Vol/(4*pi/3))^(1/3);

S = AlpertSphereSLPMat(s);
T = AlpertSphereSLPMatT(s);

% pure drag problem
rhs0 = slip_swim_vel(zeros(size(s.t)),s,1);
mu0 = S\rhs0;
trac0 = -(-eye(size(T))/2 + T)*mu0;
F0 = fJPL(zeros(size(s.t)),trac0,s,1)   % 6pi for unit sphere



% slip problem
Nc = 41;  % number of non-zero control points; this is also the degree of freedom
ss = (1:Nc)'/(Nc+1)*pi;
F = zeros(Nc,1);
slip = zeros(length(s.t),Nc);

uD = zeros(length(s.t)*2,Nc);
f = zeros(length(s.t)*2,Nc);
mu = zeros(length(s.t)*2,Nc);
A = zeros(Nc,Nc);
for k = 1:Nc
    us0 = zeros(size(ss));
    us0(k) = 1;
    us = [0;us0;0; -flip(us0);0];
    gus = geometryBspline2(us, 2*pi, []);  
    slip(:,k) = gus.r(s.t);
    uD(:,k) = slip_swim_vel(slip(:,k),s,0);
    mu(:,k) = S\uD(:,k);
    f(:,k) = -(-eye(size(T))/2 + T)*mu(:,k);    % negative of traction
    F(k) = fJPL(zeros(size(s.t)),f(:,k),s,1);
    for l = 1:k
        A(k,l) = fJPL(slip(:,k),f(:,l),s,0);
        A(l,k) = A(k,l);
    end
end

AA = [A, -F;
    -F', 0];
RHS = [-F; F0];
temp = AA\RHS;

u_opt = slip*temp(1:end-1);
f_opt = f*temp(1:end-1);
JPL = fJPL(u_opt,f_opt,s,1)
% JPL_scaled = JPL/r_scaled

%%
figure(2); hold on
plot(s.t/pi,u_opt,'r');
plot(s.t/pi,flip(u_opt),'k');

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function u = slip_swim_vel(slip, s, U)
if nargin<3
    U = 1;
end
u = [slip; slip].*[real(s.tang);imag(s.tang)]+[zeros(size(s.t));U*ones(size(s.t))];
end

function J = fJPL(slip, f, s, U)
if nargin<4
    U = 1;
end

J = (real(s.x).*s.ws(:))'*(f(1:end/2).*real(s.tang).*slip + f(end/2+1:end).*(imag(s.tang).*slip+U))*2*pi;

end

