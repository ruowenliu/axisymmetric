function [val,grad] = obj_LAMBDA_perturb_noraml(xi_geom,param)
% the value and gradient of objective (Augmented Lagragian)
% objective: -LAMBDA (minimize)
% constraints: 3D volume and surface area
% variable: xi_geom is coeffecients for the normal perturbation thata_n
% 12-03-2020, Ruowen


global iterations Solver

quadrp_perturb_normal(s_old, xi_geom, param, eta_step)

[s, ~, ~] = vec_to_quadrp_full(xi_geom, param); %%%%

[s.xi_slip, s.LAMBDA] = find_optimal_slip(s, param);

controlpoint = [0;s.xi_slip;0; -flip(s.xi_slip);0];
usfunc = uslipBSP_prd(controlpoint);
s.uslip = usfunc(s.t);
clear forwardsolve_nnf
s = forwardsolve_nnf(s,'forward','adjoint','nobasis');

C_vol = s.vol-param.vol_target;
C_area = s.area-param.area_target;

lam = iterations.lam(end,:);
sig = iterations.sig(end);

val = - s.LAMBDA - lam(1)*C_vol - lam(2)*C_area ...
    +0.5*sig*(C_vol^2+C_area^2);  % maximize LAMBDA, minimize -LAMBDA

grad = zeros(size(xi_geom));
for k=1:length(xi_geom)
    xi1 = xi_geom; xi1(k) = xi1(k)+1;
    [s1, ~, ~] = vec_to_quadrp_full(xi1, param);
    tht = s1.x-s.x;
    sensAL = ShapeSens_augL(s, tht, param, s.LAMBDA, s.xi_slip);
    grad(k) = sensAL;
end

Solver.s = s;
Solver.vol = s.vol;
Solver.area = s.area;
Solver.xi_slip = s.xi_slip;
Solver.C_vol = C_vol;
Solver.C_area = C_area;

end