function [val,grad] = obj_LAMBDA(xi_geom,param,s_now)
% the value and gradient of objective (Augmented Lagragian)
% objective: -LAMBDA (minimize)
% constraints: 3D volume and surface area
% normal perturbation
% 12-14-2020, Ruowen

global iterations Solver

s.x = s_now.x + param.MatT * xi_geom .* s_now.nperturb;
% generate quadrature
s.p = s_now.p; s.tpan = s_now.tpan; s.tlo = s_now.tlo; s.thi = s_now.thi; s.np = s_now.np;
s = quadrp_givenx(s, param.N, 'p', 'G');
s = arclen_quadp(s);

[s.xi_slip, s.LAMBDA] = find_optimal_slip(s, param);

controlpoint = [0;s.xi_slip;0; -flip(s.xi_slip);0];
usfunc = uslipBSP_prd(controlpoint);
s.uslip = usfunc(s.t);
s = forwardsolve_nnf(s,'forward','adjoint','nobasis');

C_vol = s.vol-param.vol_target;
C_area = s.area-param.area_target;

lam = iterations.lam(end,:);
sig = iterations.sig(end);

val = - s.LAMBDA - lam(1)*C_vol - lam(2)*C_area ...
    +0.5*sig*(C_vol^2+C_area^2);  % maximize LAMBDA, minimize -LAMBDA

grad = zeros(size(xi_geom));
for k=1:length(xi_geom)
    xi1 = zeros(size(xi_geom)); xi1(k) = xi1(k)+1;    
    tht = param.MatT * xi1 .* s_now.nperturb;
    grad(k) = ShapeSens_augL(s, tht, param, s.LAMBDA, s.xi_slip, lam, sig, C_vol, C_area);
end

Solver = s;
Solver.C_vol = C_vol;
Solver.C_area = C_area;

end