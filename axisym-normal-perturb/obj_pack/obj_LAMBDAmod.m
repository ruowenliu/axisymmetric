function [val,grad] = obj_LAMBDAmod(xi_reduce,param)
% the value and gradient of objective (Augmented Lagragian)
% objective: -LAMBDA (minimize)
% constraints: 3D volume and surface area
% normal perturbation
% add conditions z'(0)=z'(pi)=0, reduce degree of freedom by 2
% 1-1-2021, Ruowen

global iterations Solver

s = geom_ptb_n(xi_reduce, param);

[s.xi_slip, s.LAMBDA] = find_optimal_slip(s, param);

controlpoint = [0;s.xi_slip;0; -flip(s.xi_slip);0];
usfunc = uslipBSP_prd(controlpoint);
s.uslip = usfunc(s.t);
s = forwardsolve_nnf(s,'forward','adjoint','nobasis');

C_vol = s.vol-param.vol_target;
C_area = s.area-param.area_target;

lam = iterations.lam(end,:);
sig = iterations.sig(end);

% val = - s.LAMBDA - lam(1)*C_vol - lam(2)*C_area ...
%     +0.5*sig*(C_vol^2+C_area^2);  % maximize LAMBDA, minimize -LAMBDA

% val = - s.LAMBDA - lam(1)*C_vol - lam(2)*C_area ...
%     +0.5*sig*(C_vol^2+C_area^2) + 1e10*sum(s.x<=0);  % maximize LAMBDA, minimize -LAMBDA

val = - s.eff - lam(1)*C_vol - lam(2)*C_area ...
    +0.5*sig*(C_vol^2+C_area^2) + 1e10*sum(s.x<=0);  % maximize LAMBDA, minimize -LAMBDA
% note when F0 get close to zero, s.LAMBDA will blow up

grad = zeros(size(xi_reduce));
for k=1:length(xi_reduce)
    xi1 = xi_reduce; xi1(k) = xi1(k)+1; 
    s1 = geom_ptb_n(xi1, param);
    tht = s1.x-s.x;
    grad(k) = ShapeSens_augL(s, tht, param, s.LAMBDA, s.xi_slip, lam, sig, C_vol, C_area);
end

Solver = s;
Solver.C_vol = C_vol;
Solver.C_area = C_area;

end