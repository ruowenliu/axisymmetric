% 12-15-2020, Ruowen Liu
% This is the main program to optimize the shape and swimming strategy
% of an axissymetric micro-swimmer with given volume and given surface area
%
% package: fminunc
% method: Augmented Lagrangian
% constraints: 3D volume, surface area
% objective: efficiency
%
% perturb shape: using the theta_n along the unit normal direction at each
% quadrature point
%
%

%%
% preset
close all
clear
clear global
clear geometryBSP_prd
clear uslipBSP_prd
clear plot_bodyshape
clear forwardsolve_nnf
format long
format compact

global iterations Solver

%%
% set parameters

param.mu = 1;
param.p = 10; % p is the number of points in one panel
param.pnum_half = 30; 
param.N = param.pnum_half*param.p;

param.shapeDoF = 30;  % this number is m+1=MG+5
param.slipDoF = 15; % degree of freedom for u_slip

%%
% set Augmented Lagrangian Method parameters
sig = 10;
lam = [0,0];

%%
% set initial geometry - a perfect prolate
s.Z = @(t) 0.5*cos(-t+pi/2) + 1i*1*sin(-t+pi/2);
% s.Zp = @(t) 0.5*sin(-t+pi/2) - 1i*1*cos(-t+pi/2);
% s.Zpp = @(t) -0.5*cos(-t+pi/2) - 1i*1*sin(-t+pi/2);
s.p = param.p;
s.tpan = (0:pi/param.pnum_half:pi)';
[s, ~, ~] = quadrp(s, param.N, 'p', 'G');

s.vol = body_volume(s);
s.area = body_area(s);
param.vol_target = s.vol;
param.area_target = s.area;

%%
% Test the vector xi_geom
MG = param.shapeDoF-5;

n3_0 = imag(interpolate_by_panel(s, s.nx, 0));
n3_pi = imag(interpolate_by_panel(s, s.nx, pi));
nxp = Deriv_panel(s.nx,s);
n3p_0 = imag(interpolate_by_panel(s, nxp, 0));
n3p_pi = imag(interpolate_by_panel(s, nxp, pi));

phi_0 = Bsp5_scale_shift(0, (-5):(MG-1), MG, pi)';
phi_pi = Bsp5_scale_shift(pi, (-5):(MG-1), MG, pi)';
phip_0 = Bsp5_scale_shift_deriv(0, (-5):(MG-1), MG, pi)';
phip_pi = Bsp5_scale_shift_deriv(pi, (-5):(MG-1), MG, pi)';

xi_geom_hat_0 = zeros(param.shapeDoF,1);
xi_geom_hat_m = zeros(param.shapeDoF,1);
for id = 2:(param.shapeDoF-1)
    xi_geom_hat = zeros(param.shapeDoF,1);
    xi_geom_hat(id) = 1;
    % solve for xi_geom_hat_0 and xi_geom_hat_m
    RightVec = -[xi_geom_hat(id)*(phip_0(id)*n3_0+phi_0(id)*n3p_0); xi_geom_hat(id)*(phip_pi(id)*n3_pi+phi_pi(id)*n3p_pi)];
    LeftMat = [phip_0(1)*n3_0+phi_0(1)*n3p_0, phip_0(end)*n3_0+phi_0(end)*n3p_0; phip_pi(1)*n3_pi+phi_pi(1)*n3p_pi, phip_pi(end)*n3_pi+phi_pi(end)*n3p_pi];
    xi_geom_hat_0m = LeftMat\RightVec;   
    xi_geom_hat_0(id) = xi_geom_hat_0m(1);
    xi_geom_hat_m(id) = xi_geom_hat_0m(2);
end

% set the vector xi_geom
% xi_geom = zeros(param.shapeDoF,1);
rng(1)
xi_geom = -[0;-0.2+0.4*rand(param.shapeDoF-2,1);0];
% xi_geom = zeros(param.shapeDoF,1) + [0;0.1*rand(param.shapeDoF-2,1);0];
% xi_geom(1) = -26; xi_geom(2) = 1;
% xi_geom = zeros(param.shapeDoF,1) + [0;0;0;0;0;-0.1*rand(param.shapeDoF/2-5,1);0.1*rand(param.shapeDoF/2-5,1);0;0;0;0;0];

xi_geom(1) = xi_geom'*xi_geom_hat_0;
xi_geom(end) = xi_geom'*xi_geom_hat_m;

param.MatT = Bsp5_scale_shift(s.t, (-5):(MG-1), MG, pi); % if s.t invariantm then Mat_B_spline invarient


nperturb = s.nx;

s2.x = s.x + 0.5* param.MatT * xi_geom .* nperturb;
s2.p = s.p; s2.tpan = s.tpan; s2.tlo = s.tlo; s2.thi = s.thi; s2.np = s.np;

figure, plot(param.MatT * xi_geom)
figure, hold on, plot_bodyshape(s,[],[1,0,0])
plot_bodyshape(s2,[],[0,1,0])

% generate quadrature for s2
s2.p = s.p; s2.tpan = s.tpan; s2.tlo = s.tlo; s2.thi = s.thi; s2.np = s.np;
s2 = quadrp_givenx(s2, param.N, 'p', 'G');
s2 = arclen_quadp(s2);
tic
[s2.xi_slip, s2.LAMBDA] = find_optimal_slip(s2, param);
controlpoint2 = [0;s2.xi_slip;0; -flip(s2.xi_slip);0];
usfunc2 = uslipBSP_prd(controlpoint2);
s2.uslip = usfunc2(s2.t);
s2 = forwardsolve_nnf(s2,'forward','adjoint','nobasis');
toc
tic
[s.xi_slip, s.LAMBDA] = find_optimal_slip(s, param);
controlpoint = [0;s.xi_slip;0; -flip(s.xi_slip);0];
usfunc = uslipBSP_prd(controlpoint);
s.uslip = usfunc(s.t);
s = forwardsolve_nnf(s,'forward','adjoint','nobasis');
toc
%% 
figure, hold on, plot_bodyshape(s,[],[1,0,0])
% plot_bodyshape(s2,[],[0,1,0])
keyboard
%%
% optimze

% data
iterations.vol = [];
iterations.area = [];
iterations.xi_geom = [];
iterations.xi_slip = [];
iterations.Solver = [];
iterations.stepsize = [];
iterations.iternum = [];
iterations.funcCount = [];

% set options for fminunc
options = optimoptions(@fminunc,...
    'Algorithm','quasi-newton',...
    'HessUpdate','bfgs',...
    'StepTolerance',1e-10,...     
    'SpecifyObjectiveGradient',true,...
    'CheckGradients',false,...
    'FunValCheck','on',...
    'Display','iter',...
    'OptimalityTolerance',1e-2,...
    'OutputFcn',@output_axisym);

% run optimization
stop_opt = 0; loop_counter = 0;
s_now = s; s_now.nperturb=s_now.nx;
xi_geom = zeros(size(xi_geom));
%%
while stop_opt == 0  
if ~isfield(iterations, 'ALMnum')   % initialize Augmented Lagrangian param
    iterations.ALMnum = 1;
    iterations.stepsize = 0;
    % prepare for optimization
    iterations.lam = [0,0];
    if ~isfield(iterations, 'sig')
        iterations.sig = 10;
    end
    iterations.constraints_tol = 1./(iterations.sig(end).^0.1); % tolerance for constraints
    iterations.convergence_tol = 0.001;   % convergence tolerance
    % tab is used to show update whether multiplier or penalty
    iterations.tab = {'I'}; % initial
else
    maxconstval = max([abs(Solver.C_vol),abs(Solver.C_area)]);
    if maxconstval < iterations.constraints_tol(end)
        if maxconstval < iterations.convergence_tol(end)
            fprintf('---\nOptimization is completed because the constraints are satisfied.\n---\n')
            stop_opt = 1;
        else
            % Update multipliers, tighten tolerances            
            iterations.ALMnum = [iterations.ALMnum; iterations.ALMnum(end)+1];
            iterations.stepsize = [iterations.stepsize; 0];
            iterations.convergence_tol = [iterations.convergence_tol; iterations.convergence_tol(end)];
            iterations.lam = [iterations.lam; NaN, NaN];
            iterations.lam(end,1) = iterations.lam(end-1,1)-iterations.sig(end).*Solver.C_vol;
            iterations.lam(end,2) = iterations.lam(end-1,2)-iterations.sig(end).*Solver.C_area;
            iterations.constraints_tol = [iterations.constraints_tol; NaN];
            iterations.constraints_tol(end) = iterations.constraints_tol(end-1)./(iterations.sig(end)^0.9);
            iterations.tab = [iterations.tab; 'M']; % update multiplier
            iterations.sig = [iterations.sig; iterations.sig(end)]; % keep same penalty
        end
    else
        % Increase penalty parameter, tighten tolerances
        iterations.ALMnum = [iterations.ALMnum; iterations.ALMnum(end)+1];
        iterations.stepsize = [iterations.stepsize; 0];
        iterations.convergence_tol = [iterations.convergence_tol; iterations.convergence_tol(end)];
        iterations.sig = [iterations.sig; NaN];
        iterations.sig(end) = 10*iterations.sig(end-1);
        iterations.constraints_tol = [iterations.constraints_tol; NaN];
        iterations.constraints_tol(end) = 1./(iterations.sig(end).^0.1);
        iterations.tab = [iterations.tab; 'P'];  % update penalty
        iterations.lam = [iterations.lam; iterations.lam(end,:)]; % keep same multiplier
%         s_now = Solver;
%         xi_geom = zeros(size(xi_geom));
    end    
end

% keyboard
if stop_opt == 0    
    % --------------- find max eff by fminunc --------------- 
%     % step by step Gradien Descent
%     stepsize_iter = 0.05;
%     num_iter = 10;
%     for k = 1:num_iter
%         tic
%         [s.xi_slip, s.LAMBDA] = find_optimal_slip(s, param);
%         toc
%         xi_geom = zeros(size(xi_geom));
%         grad_xi_geom = zeros(size(xi_geom));
%         tic
%         for kn=1:length(xi_geom)
%             xi1 = xi_geom; xi1(kn) = xi1(kn)+1;
%             tht = Bsp5_scale_shift(s.t, (-5):(MG-1), MG, pi)*xi1 .* s.nx; % theta_s is zero
%             grad_xi_geom(k) = ShapeSens_augL(s, tht, param, s.LAMBDA, s.xi_slip);
%         end
%         toc
%         xi_geom = xi_geom - stepsize_iter * grad_xi_geom;
%         s_new = quadrp_perturb_normal(s, xi_geom, param);
%         s = s_new;
%     end
    func = @(x) obj_LAMBDA(x,param,s_now);
    [xi_geom,fval,exitflag,output] = fminunc(func,xi_geom,options);
%     while Solver.relocate == 1
%         xi_geom = relocate_vec(xi_geom,param);
%         [xi_geom,fval,exitflag,output] = fminunc(func,xi_geom,options);
%     end
%         
%     if iterations.gradient_max(end) > 1e8
%         fprintf('--\ngradient too large.\n--\n')
%         %         keyboard
%     end
    if loop_counter >= 8
        stop_opt = -1;
        fprintf('--\n Warning: loop_counter too large. \n--\n')
        %         keyboard
    end
end

end


% ------------------------------------------------------------------------- 
% save results

save('./output/test_1218.mat')
% nowtime = datestr(now,'yyyymmddHHMM');
% 
% dataname = ['./output/' 'data' nowtime '.mat'];
% fprintf('data name: %s \n', dataname)
% save(dataname)

%%



