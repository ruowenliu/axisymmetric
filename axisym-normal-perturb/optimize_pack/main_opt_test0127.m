% 01-27-2021, Ruowen Liu
% This is the main program to optimize the shape and swimming strategy
% of an axissymetric micro-swimmer with given volume and given surface area
%
% package: fminunc
% method: Augmented Lagrangian
% constraints: 3D volume, surface area
% objective: efficiency
%
% perturb shape: using the theta_n along the unit normal direction at each
% quadrature point
% modification: enforce z'(0) = z'(pi) = 0
% This modification reduces the number of DoF for geometry
%
% Test sent to the GreatLakes
%

%%
% preset
close all
clear
clear global
clear geometryBSP_prd
clear uslipBSP_prd
clear plot_bodyshape
clear forwardsolve_nnf
format long
format compact

warning off

global iterations Solver s_base n_perturb flag
flag = 1;
%%
% set parameters

param.mu = 1;
param.p = 10; % p is the number of points in one panel
param.pnum_half = 60; 
param.N = param.pnum_half*param.p;

param.mval = 28;
param.shapeDoF = param.mval+1;  % this number is m+1=MG+5
param.shapeDoFmod = param.shapeDoF-2;  % degree of freedom for geometry
% param.shapeDoFmod = param.shapeDoF-4;  % degree of freedom for geometry
param.slipDoF = 40; % degree of freedom for u_slip

%%
% set initial geometry - a perfect prolate
s0.Z = @(t) 0.63*cos(-t+pi/2) + 1i*0.63*4*sin(-t+pi/2);
% s.Zp = @(t) 0.5*sin(-t+pi/2) - 1i*1*cos(-t+pi/2);
% s.Zpp = @(t) -0.5*cos(-t+pi/2) - 1i*1*sin(-t+pi/2);
s0.p = param.p;
s0.tpan = (0:pi/param.pnum_half:pi)';
[s0, ~, ~] = quadrp(s0, param.N, 'p', 'G');

s = s0;
% load s_gallery.mat
% s = s_gallery{1,1};
% % s.p = s0.p; s.tpan = s0.tpan; s.tlo = s0.tlo; s.thi = s0.thi; s.np = s0.np;
% % s.x = flip(interpolate_by_panel(s, s.x, s0.t));
% s.x = interpolate_by_panel(s, s.x, s0.t);
% 
% s.p = s0.p; s.tpan = s0.tpan; s.tlo = s0.tlo; s.thi = s0.thi; s.np = s0.np;
% s = quadrp_givenx(s, param.N, 'p', 'G');

figure, plot(real(s.x),imag(s.x))
hold on, plot(real(s.x(1)),imag(s.x(1)), '*')
quiver(real(s.x),imag(s.x), real(s.tang),imag(s.tang))
quiver(real(s.x),imag(s.x), real(s.nx),imag(s.nx))
axis equal

% plot_bodyshape(s0)

% keyboard

s.vol = body_volume(s);
s.area = body_area(s);
param.vol_target = s.vol;
param.area_target = s.area;

MG = param.shapeDoF-5;
param.MatT = Bsp5_scale_shift(s.t, (-5):(MG-1), MG, pi); % if s.t invariant then Mat_B_spline invarient



%%
% rng(1)

% s_base = s; n_perturb = s.nx;
% xi_reduce = 0.02+0.1*rand(param.shapeDoFmod,1);
% s2 = geom_ptb_n(xi_reduce, param);
% figure, hold on, plot_bodyshape(s,[],[1,0,0]),plot_bodyshape(s2,[],[0,1,0])
% 
% % myfun = @(x) xi_find_1(x, xi_reduce, param);
% % fzero(myfun, 0)
% % myfun = @(x) xi_find_end(x, xi_reduce, param);
% % fzero(myfun, 0)
% % tic
% % [s2.xi_slip, s2.LAMBDA] = find_optimal_slip(s2, param);
% % controlpoint2 = [0;s2.xi_slip;0; -flip(s2.xi_slip);0];
% % usfunc2 = uslipBSP_prd(controlpoint2);
% % s2.uslip = usfunc2(s2.t);
% % s2 = forwardsolve_nnf(s2,'forward','adjoint','nobasis');
% % toc
% 
% s_base = s2; n_perturb = s2.nx;
% xi_reduce2 = -0.02+0.001*rand(param.shapeDoFmod,1);
% s3 = geom_ptb_n(xi_reduce2, param);
% hold on, plot_bodyshape(s3,[],[0,0,1])
% 
% myfun = @(x) xi_find_end(x, xi_reduce2, param);
% fzero(myfun, 0)
% tic
% [s3.xi_slip, s3.LAMBDA] = find_optimal_slip(s3, param);
% controlpoint3 = [0;s3.xi_slip;0; -flip(s3.xi_slip);0];
% usfunc3 = uslipBSP_prd(controlpoint3);
% s3.uslip = usfunc3(s3.t);
% s3 = forwardsolve_nnf(s3,'forward','adjoint','nobasis');
% toc
% keyboard

%%
% optimze

% data
iterations.vol = [];
iterations.area = [];
iterations.xi_reduce = [];
iterations.xi_slip = [];
iterations.Solver = [];
iterations.stepsize = [];
iterations.iternum = [];
iterations.funcCount = [];

% set options for fminunc
options = optimoptions(@fminunc,...
    'Algorithm','quasi-newton',...
    'HessUpdate','bfgs',...
    'StepTolerance',1e-10,...     
    'SpecifyObjectiveGradient',true,...
    'CheckGradients',false,...
    'FunValCheck','on',...
    'Display','iter',...
    'OptimalityTolerance',1e-2,...
    'OutputFcn',@output_axisym_mod);

% run optimization
stop_opt = 0; loop_counter = 0;

s_base = s; n_perturb = s.nx;
xi_reduce = zeros(param.shapeDoFmod,1);
% xi_reduce(10) = 1;
% snew = geom_ptb_n(xi_reduce, param);
% figure, hold on, plot_bodyshape(s,[],[1,0,0]),plot_bodyshape(snew,[],[0,1,0])
% keyboard
%%
while stop_opt == 0  
    loop_counter = loop_counter+1;
if ~isfield(iterations, 'ALMnum')   % initialize Augmented Lagrangian param
    iterations.ALMnum = 1;
    iterations.stepsize = 0;
    % prepare for optimization
    iterations.lam = [0,0];
    if ~isfield(iterations, 'sig')
        iterations.sig = 100;
    end
    iterations.constraints_tol = 1./(iterations.sig(end).^0.1); % tolerance for constraints
    iterations.convergence_tol = 0.01;   % convergence tolerance
    % tab is used to show update whether multiplier or penalty
    iterations.tab = {'I'}; % initial
else
    maxconstval = max([abs(Solver.C_vol),abs(Solver.C_area)]);
    if maxconstval < iterations.constraints_tol(end)
        if maxconstval < iterations.convergence_tol(end)
            fprintf('---\nOptimization is completed because the constraints are satisfied.\n---\n')
            stop_opt = 1;
        else
            % Update multipliers, tighten tolerances            
            iterations.ALMnum = [iterations.ALMnum; iterations.ALMnum(end)+1];
            iterations.stepsize = [iterations.stepsize; 0];
            iterations.convergence_tol = [iterations.convergence_tol; iterations.convergence_tol(end)];
            iterations.lam = [iterations.lam; NaN, NaN];
            iterations.lam(end,1) = iterations.lam(end-1,1)-iterations.sig(end).*Solver.C_vol;
            iterations.lam(end,2) = iterations.lam(end-1,2)-iterations.sig(end).*Solver.C_area;
            iterations.constraints_tol = [iterations.constraints_tol; NaN];
            iterations.constraints_tol(end) = iterations.constraints_tol(end-1)./(iterations.sig(end)^0.9);
            iterations.tab = [iterations.tab; 'M']; % update multiplier
            iterations.sig = [iterations.sig; iterations.sig(end)]; % keep same penalty
        end
    else
        % Increase penalty parameter, tighten tolerances
        iterations.ALMnum = [iterations.ALMnum; iterations.ALMnum(end)+1];
        iterations.stepsize = [iterations.stepsize; 0];
        iterations.convergence_tol = [iterations.convergence_tol; iterations.convergence_tol(end)];
        iterations.sig = [iterations.sig; NaN];
        iterations.sig(end) = 10*iterations.sig(end-1);
        iterations.constraints_tol = [iterations.constraints_tol; NaN];
        iterations.constraints_tol(end) = 1./(iterations.sig(end).^0.1);
        iterations.tab = [iterations.tab; 'P'];  % update penalty
        iterations.lam = [iterations.lam; iterations.lam(end,:)]; % keep same multiplier
    end    
end

% keyboard
if stop_opt == 0    
    % --------------- find max eff by fminunc --------------- 
    func = @(x) obj_LAMBDAmod(x,param);
    
    while flag == 1
    [xi_reduce,fval,exitflag,output] = fminunc(func,xi_reduce,options);
    % --- update n_perturb ---
    n_perturb = Solver.nx;
    s_base = Solver;
    xi_reduce = zeros(param.shapeDoFmod,1);
    flag = 1;  %%%
    end

    flag = 1;

    stop_opt = 1; %%%
    
    if loop_counter >= 8
        stop_opt = -1;
        fprintf('--\n Warning: loop_counter too large. \n--\n')
        %         keyboard
    end
end

end


% ------------------------------------------------------------------------- 
% save results

save('./slender_0127_p60m28us40cvol01.mat')
% nowtime = datestr(now,'yyyymmddHHMM');
% 
% dataname = ['./output/' 'data' nowtime '.mat'];
% fprintf('data name: %s \n', dataname)
% save(dataname)

%%



