% 01-27-2021, Ruowen Liu
% This is a program for testing
% optimize the shape and swimming strategy
% of an axissymetric micro-swimmer with given volume and given surface area
%
% package: fminunc
% method: Augmented Lagrangian
% constraints: 3D volume, surface area
% objective: efficiency
%
% perturb shape: using the theta_n along the unit normal direction at each
% quadrature point
% modification: enforce z'(0) = z'(pi) = 0
% This modification reduces the number of DoF for geometry
%
% Test sent to the GreatLakes
%

%%
% preset
close all
clear
clear global
clear geometryBSP_prd
clear uslipBSP_prd
clear plot_bodyshape
clear forwardsolve_nnf
format long
format compact

warning off

% global iterations Solver 
global s_base n_perturb 
% gloabl flag
% flag = 1;

% set parameters

param.mu = 1;
param.p = 10; % p is the number of points in one panel
param.pnum_half = 60; 
param.N = param.pnum_half*param.p;

param.mval = 28;
param.shapeDoF = param.mval+1;  % this number is m+1=MG+5
param.shapeDoFmod = param.shapeDoF-2;  % degree of freedom for geometry
param.slipDoF = 40; % degree of freedom for u_slip

% set initial geometry - a perfect prolate
s0.Z = @(t) 0.63*cos(-t+pi/2) + 1i*0.63*4*sin(-t+pi/2);
s0.p = param.p;
s0.tpan = (0:pi/param.pnum_half:pi)';
[s0, ~, ~] = quadrp(s0, param.N, 'p', 'G');

s = s0;
% load s_gallery.mat
% s = s_gallery{1,1};
% % s.p = s0.p; s.tpan = s0.tpan; s.tlo = s0.tlo; s.thi = s0.thi; s.np = s0.np;
% % s.x = flip(interpolate_by_panel(s, s.x, s0.t));
% s.x = interpolate_by_panel(s, s.x, s0.t);
% 
% s.p = s0.p; s.tpan = s0.tpan; s.tlo = s0.tlo; s.thi = s0.thi; s.np = s0.np;
% s = quadrp_givenx(s, param.N, 'p', 'G');

figure, plot(real(s.x),imag(s.x))
hold on, plot(real(s.x(1)),imag(s.x(1)), '*')
quiver(real(s.x),imag(s.x), real(s.tang),imag(s.tang))
quiver(real(s.x),imag(s.x), real(s.nx),imag(s.nx))
axis equal
saveas(gcf, './image/fig0127_1.m')

s.vol = body_volume(s);
s.area = body_area(s);
param.vol_target = s.vol;
param.area_target = s.area;

MG = param.shapeDoF-5;
param.MatT = Bsp5_scale_shift(s.t, (-5):(MG-1), MG, pi); % if s.t invariant then Mat_B_spline invarient

rng(1)
s_base = s; n_perturb = s.nx;
xi_reduce = 0.02+0.1*rand(param.shapeDoFmod,1);
s2 = geom_ptb_n(xi_reduce, param);
figure, hold on, plot_bodyshape(s,[],[1,0,0]),plot_bodyshape(s2,[],[0,1,0])

s_base = s2; n_perturb = s2.nx;
xi_reduce2 = -0.02+0.001*rand(param.shapeDoFmod,1);
s3 = geom_ptb_n(xi_reduce2, param);
hold on, plot_bodyshape(s3,[],[0,0,1])
saveas(gcf, './image/fig0127_2.m')

myfun = @(x) xi_find_end(x, xi_reduce2, param);
fzero(myfun, 0)
tic
[s3.xi_slip, s3.LAMBDA] = find_optimal_slip(s3, param);
controlpoint3 = [0;s3.xi_slip;0; -flip(s3.xi_slip);0];
usfunc3 = uslipBSP_prd(controlpoint3);
s3.uslip = usfunc3(s3.t);
s3 = forwardsolve_nnf(s3,'forward','adjoint','nobasis');
toc

% ------------------------------------------------------------------------- 
% save results

save('./output/test0127.mat')
saveas(gcf, './image/fig0127_1.m')
% nowtime = datestr(now,'yyyymmddHHMM');
% dataname = ['./output/' 'data' nowtime '.mat'];
% fprintf('data name: %s \n', dataname)
% save(dataname)

close all

