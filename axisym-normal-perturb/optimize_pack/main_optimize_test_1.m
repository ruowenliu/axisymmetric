% 11-19-2020, Ruowen
% This is the main program to optimize the shape and swimming strategy
% of an axissymetric micro-swimmer with given volume and given surface area
%
% package: fminunc
% method: Augmented Lagrangian
% constraints: 3D volume, surface area
% objective: efficiency
%
%
%

%%
% preset
close all
clear
clear global
clear geometryBSP_prd
clear uslipBSP_prd
clear plot_bodyshape
format long
format compact

global iterations

%%
% set parameters

param.mu = 1;
param.p = 10; % p is the number of points in one panel
param.panel_num = 30*2; % panel_num is for a full cycle, panel_num/2 is the num of panels for the generating arc (halp cycle, top to bottom)
param.N = param.panel_num*param.p;
param.xtop = 0; param.xbot = 0;  % must equal zero

param.shapeDoF = 11;  % this number is M+1 (all grid points)
% the total degree of freedom for geometry is ( shapeDoF + (shapeDoF-2) )
param.slipDoF = 15; % degree of freedom for u_slip

%%
% set Augmented Lagrangian Method parameters
sig = 10;
lam = [0,0];

%%
% set current geometry parameter vector geom_xi


r1 = 1;
r2 = 0.8;
x = r1*cos(pi/2-linspace(0,pi,param.shapeDoF))';
z = r2*sin(pi/2-linspace(0,pi,param.shapeDoF))';
xi_geom = [x(2:end-1);z(1:end)]; % length: shapeNc*2-2
rng(1)
xi_geom = xi_geom + 0.04*randn(size(xi_geom));

[s, ~, ~] = vec_to_quadrp_full(xi_geom, param);
s = arclen(s);
figure(1), plot_bodyshape(s,[],[1,0,0]), title('test (initial) shape')

param.vol_target = s.vol;
param.area_target = s.area;

%%
% compute optimal slip velocity for current geometry
% tic
% [s.xi_slip, s.LAMBDA] = find_optimal_slip(s, param);
% toc

%%
% optimze

% data
iterations.vol = [];
iterations.area = [];
iterations.xi_geom = [];
iterations.xi_slip = [];
iterations.Solver = [];
iterations.stepsize = [];
iterations.iternum = [];
iterations.funcCount = [];

% set options for fminunc
options = optimoptions(@fminunc,...
    'Algorithm','quasi-newton',...
    'HessUpdate','bfgs',...
    'StepTolerance',1e-10,...     
    'SpecifyObjectiveGradient',true,...
    'CheckGradients',false,...
    'FunValCheck','on',...
    'Display','iter',...
    'OptimalityTolerance',1e-2,...
    'OutputFcn',@output_axisym);

% run optimization
stop_opt = 0; loop_counter = 0;

while stop_opt == 0  
if ~isfield(iterations, 'ALMnum')   % initialize Augmented Lagrangian param
    iterations.ALMnum = 1;
    iterations.stepsize = 0;
    % prepare for optimization
    iterations.lam = [0,0];
    if ~isfield(iterations, 'sig')
        iterations.sig = 10;
    end
    iterations.constraints_tol = 1./(iterations.sig(end).^0.1); % tolerance for constraints
    iterations.convergence_tol = 0.001;   % convergence tolerance
    % tab is used to show update whether multiplier or penalty
    iterations.tab = {'I'}; % initial
else
    maxconstval = max([abs(Solver.C_vol),abs(Solver.C_area)]);
    if maxconstval < iterations.constraints_tol(end)
        if maxconstval < iterations.convergence_tol(end)
            fprintf('---\nOptimization is completed because the constraints are satisfied.\n---\n')
            stop_opt = 1;
        else
            % Update multipliers, tighten tolerances            
            iterations.ALMnum = [iterations.ALMnum; iterations.ALMnum(end)+1];
            iterations.stepsize = [iterations.stepsize; 0];
            iterations.convergence_tol = [iterations.convergence_tol; iterations.convergence_tol(end)];
            iterations.lam = [iterations.lam; NaN, NaN];
            iterations.lam(end,1) = iterations.lam(end-1,1)-iterations.sig(end).*Solver.C_vol;
            iterations.lam(end,2) = iterations.lam(end-1,2)-iterations.sig(end).*Solver.C_area;
            iterations.constraints_tol = [iterations.constraints_tol; NaN];
            iterations.constraints_tol(end) = iterations.constraints_tol(end-1)./(iterations.sig(end)^0.9);
            iterations.tab = [iterations.tab; 'M']; % update multiplier
            iterations.sig = [iterations.sig; iterations.sig(end)]; % keep same penalty
        end
    else
        % Increase penalty parameter, tighten tolerances
        iterations.ALMnum = [iterations.ALMnum; iterations.ALMnum(end)+1];
        iterations.stepsize = [iterations.stepsize; 0];
        iterations.convergence_tol = [iterations.convergence_tol; iterations.convergence_tol(end)];
        iterations.sig = [iterations.sig; NaN];
        iterations.sig(end) = 10*iterations.sig(end-1);
        iterations.constraints_tol = [iterations.constraints_tol; NaN];
        iterations.constraints_tol(end) = 1./(iterations.sig(end).^0.1);
        iterations.tab = [iterations.tab; 'P'];  % update penalty
        iterations.lam = [iterations.lam; iterations.lam(end,:)]; % keep same multiplier
    end    
end


if stop_opt == 0    
    % --------------- find max eff  --------------- 
    func = @(x) obj_LAMBDA(x,param);
    [xi_geom,fval,exitflag,output] = fminunc(func,xi_geom,options);
    while Solver.relocate == 1
        xi_geom = relocate_vec(xi_geom,param);
        [xi_geom,fval,exitflag,output] = fminunc(func,xi_geom,options);
    end
        
    if iterations.gradient_max(end) > 1e8
        fprintf('--\ngradient too large.\n--\n')
        %         keyboard
    end
    if loop_counter >= 8
        stop_opt = -1;
        fprintf('--\n Warning: loop_counter too large. \n--\n')
        %         keyboard
    end
end

end


% -------------------------------------------------------------------------
%% 4. save results

nowtime = datestr(now,'yyyymmddHHMM');

dataname = ['./output/' 'data' nowtime '.mat'];
fprintf('data name: %s \n', dataname)
save(dataname)





