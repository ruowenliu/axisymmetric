% 12-14-2020, Ruowen Liu
% This is the main program to optimize the shape and swimming strategy
% of an axissymetric micro-swimmer with given volume and given surface area
%
% package: fminunc
% method: Augmented Lagrangian
% constraints: 3D volume, surface area
% objective: efficiency
%
% perturb shape: using the theta_n along the unit normal direction at each
% quadrature point
%
%

%%
% preset
% load()

global iterations Solver

%%
% optimze
% run optimization
stop_opt = 0; loop_counter = 0;
s_now = Solver;
xi_geom = zeros(size(xi_geom));
%%
while stop_opt == 0  
if ~isfield(iterations, 'ALMnum')   % initialize Augmented Lagrangian param
    iterations.ALMnum = 1;
    iterations.stepsize = 0;
    % prepare for optimization
    iterations.lam = [0,0];
    if ~isfield(iterations, 'sig')
        iterations.sig = 10;
    end
    iterations.constraints_tol = 1./(iterations.sig(end).^0.1); % tolerance for constraints
    iterations.convergence_tol = 0.001;   % convergence tolerance
    % tab is used to show update whether multiplier or penalty
    iterations.tab = {'I'}; % initial
else
    maxconstval = max([abs(Solver.C_vol),abs(Solver.C_area)]);
    if maxconstval < iterations.constraints_tol(end)
        if maxconstval < iterations.convergence_tol(end)
            fprintf('---\nOptimization is completed because the constraints are satisfied.\n---\n')
            stop_opt = 1;
        else
            % Update multipliers, tighten tolerances            
            iterations.ALMnum = [iterations.ALMnum; iterations.ALMnum(end)+1];
            iterations.stepsize = [iterations.stepsize; 0];
            iterations.convergence_tol = [iterations.convergence_tol; iterations.convergence_tol(end)];
            iterations.lam = [iterations.lam; NaN, NaN];
            iterations.lam(end,1) = iterations.lam(end-1,1)-iterations.sig(end).*Solver.C_vol;
            iterations.lam(end,2) = iterations.lam(end-1,2)-iterations.sig(end).*Solver.C_area;
            iterations.constraints_tol = [iterations.constraints_tol; NaN];
            iterations.constraints_tol(end) = iterations.constraints_tol(end-1)./(iterations.sig(end)^0.9);
            iterations.tab = [iterations.tab; 'M']; % update multiplier
            iterations.sig = [iterations.sig; iterations.sig(end)]; % keep same penalty
        end
    else
        % Increase penalty parameter, tighten tolerances
        iterations.ALMnum = [iterations.ALMnum; iterations.ALMnum(end)+1];
        iterations.stepsize = [iterations.stepsize; 0];
        iterations.convergence_tol = [iterations.convergence_tol; iterations.convergence_tol(end)];
        iterations.sig = [iterations.sig; NaN];
        iterations.sig(end) = 10*iterations.sig(end-1);
        iterations.constraints_tol = [iterations.constraints_tol; NaN];
        iterations.constraints_tol(end) = 1./(iterations.sig(end).^0.1);
        iterations.tab = [iterations.tab; 'P'];  % update penalty
        iterations.lam = [iterations.lam; iterations.lam(end,:)]; % keep same multiplier
    end    
end

% keyboard
if stop_opt == 0    
    % --------------- find max eff by fminunc --------------- 
%     % step by step Gradien Descent
%     stepsize_iter = 0.05;
%     num_iter = 10;
%     for k = 1:num_iter
%         tic
%         [s.xi_slip, s.LAMBDA] = find_optimal_slip(s, param);
%         toc
%         xi_geom = zeros(size(xi_geom));
%         grad_xi_geom = zeros(size(xi_geom));
%         tic
%         for kn=1:length(xi_geom)
%             xi1 = xi_geom; xi1(kn) = xi1(kn)+1;
%             tht = Bsp5_scale_shift(s.t, (-5):(MG-1), MG, pi)*xi1 .* s.nx; % theta_s is zero
%             grad_xi_geom(k) = ShapeSens_augL(s, tht, param, s.LAMBDA, s.xi_slip);
%         end
%         toc
%         xi_geom = xi_geom - stepsize_iter * grad_xi_geom;
%         s_new = quadrp_perturb_normal(s, xi_geom, param);
%         s = s_new;
%     end
    func = @(x) obj_LAMBDA(x,param,s_now);
    [xi_geom,fval,exitflag,output] = fminunc(func,xi_geom,options);
    
    s_now = Solver;
    xi_geom = zeros(size(xi_geom));
%     while Solver.relocate == 1
%         xi_geom = relocate_vec(xi_geom,param);
%         [xi_geom,fval,exitflag,output] = fminunc(func,xi_geom,options);
%     end
%         
%     if iterations.gradient_max(end) > 1e8
%         fprintf('--\ngradient too large.\n--\n')
%         %         keyboard
%     end
    if loop_counter >= 8
        stop_opt = -1;
        fprintf('--\n Warning: loop_counter too large. \n--\n')
        %         keyboard
    end
end

end


% ------------------------------------------------------------------------- 
% save results

% save('./output/test_2_final.mat')
% nowtime = datestr(now,'yyyymmddHHMM');
% 
% dataname = ['./output/' 'data' nowtime '.mat'];
% fprintf('data name: %s \n', dataname)
% save(dataname)

%%



