function valout = xi_find_1(xi1, xi_reduce, param)
global s_base n_perturb

xi_geom = [xi1;xi_reduce;0];

s.x = s_base.x + param.MatT * xi_geom .* n_perturb;
s.p = s_base.p; s.tpan = s_base.tpan; s.tlo = s_base.tlo; s.thi = s_base.thi; s.np = s_base.np;
s = quadrp_givenx(s, param.N, 'p', 'G');
% s = arclen_quadp(s);

valout = interpolate_by_panel(s, imag(s.tang), 0);
end