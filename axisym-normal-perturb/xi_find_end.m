function valout = xi_find_end(xi_end, xi_reduce, param)
global s_base n_perturb

xi_geom = [0;xi_reduce;xi_end];

s.x = s_base.x + param.MatT * xi_geom .* n_perturb;
s.p = s_base.p; s.tpan = s_base.tpan; s.tlo = s_base.tlo; s.thi = s_base.thi; s.np = s_base.np;
s = quadrp_givenx(s, param.N, 'p', 'G');
% s = arclen_quadp(s);

valout = interpolate_by_panel(s, imag(s.tang), pi);
end