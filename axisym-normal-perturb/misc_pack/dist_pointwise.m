function distvec = dist_pointwise(a1,a2)
% Euclidean distance between a1 and a2, pointwise when they are vectors of
% complex numbers.

distvec = sqrt((real(a1)-real(a2)).^2+(imag(a1)-imag(a2)).^2);

end