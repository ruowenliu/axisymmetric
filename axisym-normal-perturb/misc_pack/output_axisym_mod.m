function stop = output_axisym_mod(xi_reduce,optimValues,state)
% This function can stop optimization at one successfull iteration step
%
% add conditions z'(0)=z'(pi)=0, reduce degree of freedom by 2
% version: 01/01/2021
% Ruowen Liu, ruowen@umich.edu

global Solver iterations s_base n_perturb flag
stop = false;

switch state
    case {'init'}
        %         fprintf('init\n')
    case {'iter'}
        % save data
        
        iterations.vol = [iterations.vol; Solver.vol];
        iterations.area = [iterations.area; Solver.area];
        iterations.xi_reduce = [iterations.xi_reduce, xi_reduce];
        iterations.xi_slip = [iterations.xi_slip, Solver.xi_slip];
        iterations.Solver = [iterations.Solver; Solver];
        iterations.stepsize = [iterations.stepsize; optimValues.stepsize];
        iterations.iternum = [iterations.iternum; optimValues.iteration];
        iterations.funcCount = [iterations.funcCount; optimValues.funccount];
        
        if optimValues.iteration~=0
            iterations.lam = [iterations.lam; iterations.lam(end,:)];
            iterations.sig = [iterations.sig; iterations.sig(end)];
            iterations.constraints_tol = [iterations.constraints_tol; iterations.constraints_tol(end)];
            iterations.convergence_tol = [iterations.convergence_tol; iterations.convergence_tol(end)];
            iterations.tab = [iterations.tab; iterations.tab{end}];
            iterations.ALMnum = [iterations.ALMnum; iterations.ALMnum(end)];
        end
        
        if optimValues.iteration~=0
            iterations.tab = [iterations.tab; [iterations.tab{end} 'RS']];
        end
        
        if optimValues.iteration==5
            stop = true;
        end
        %         x = [real(param.xtop);vec(1:(param.shapeNc-2));real(param.xbot)];
        %         z = vec((param.shapeNc-1):end);
        %         % generate x,z for the entire body.
        %         x_full = [x; -flip(x(1:end-1))];
        %         z_full = [z;flip(z(1:end-1))];
        %         mindist = min(sqrt((x_full(2:end)-x_full(1:end-1)).^2+(z_full(2:end)-z_full(1:end-1)).^2));
        %         fprintf('minimal dist = %g and percentage = %f\n',mindist, mindist/Solver.s.ell)
        %
        %         if mindist < 0.5*Solver.s.ell/(param.shapeNc-1)
        %             stop = true;
        %             Solver.relocate = 1;
        %             pause(1)
        %         else
        %             Solver.relocate = 0;
        %         end
        
        
        plot_bodyshape(Solver)
        %         save('./output/test_1.mat')
        %         save('./output/test_2.mat')
%         save('./output/test_normalperturb_1214_A2.mat')
%         save('./output/test_normalperturb_1215.mat')
%         save('./output/test_normalperturb_0101.mat')
        %         keyboard
        % update s_base
        % ---- update s_base ----
%         s_base = Solver;
        
        
    case {'done'}
        flag=0;
    otherwise
        
end
end