function [max_eigenvalue, xi_slip, EigenV, EigenD, LAMBDA] = find_max_eigenvalue(s, param)
% Find the maximal eigenvalue of the generalized eigenvalue problem

clear forwardsolve_nnf
s_mode = cell(param.slipDoF,1);
for mode = 1:param.slipDoF
    controlpoint_half = zeros(param.slipDoF,1); controlpoint_half(mode) = 1;
    controlpoint = [0;controlpoint_half;0; -flip(controlpoint_half);0];
    usfunc_eachmode = uslipBSP_prd(controlpoint);
    s_eachmode = s;
    s_eachmode.uslip = usfunc_eachmode(s.t);    
    if mode == 1
        s_eachmode = forwardsolve_nnf(s_eachmode,'noforward','adjoint','basis');
    else
        s_eachmode = forwardsolve_nnf(s_eachmode,'noforward','noadjoint','basis');
    end
    s_mode{mode, 1} = s_eachmode;
end

Amatrix = zeros(param.slipDoF,param.slipDoF);
Bmatrix = zeros(param.slipDoF,param.slipDoF);
Fvec = zeros(param.slipDoF, 1);
for p = 1:param.slipDoF
    for q = 1:p
        Amatrix(p,q) = 2*pi*s.ws'*(dotv(s_mode{p,1}.trac_k,(s_mode{q,1}.uslip.*s.tang)).*real(s.x));  
        Bmatrix(p,q) = Amatrix(p,q) - s_mode{p,1}.F_k*s_mode{q,1}.F_k/s_mode{1,1}.F0;
    end
    Fvec(p) = s_mode{p,1}.F_k;
end
Bmatrix = (Bmatrix+Bmatrix') - eye(size(Bmatrix,1)).*diag(Bmatrix);

xi_slip = Bmatrix\Fvec;
max_eigenvalue = Fvec'*xi_slip;

LAMBDA = max_eigenvalue/s_mode{1,1}.F0;

[EigenV,EigenD] = eig(Fvec*Fvec',Bmatrix);
end