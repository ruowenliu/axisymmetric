function vol = body_area(s)
% body_area gives the 3D surface area of the axis-symmetric body
% s - quadrature of the generating arc (from top to bottom, half-circle)

% add a negative sign because the curve is going downward
vol = 2*pi*s.ws'*real(s.x);
end