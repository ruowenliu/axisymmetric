% verify_interpolation

% generate a shape, ellipse, analytical results are known
s.Z = @(t) 0.8*cos(-t+pi/2) + 1i*1*sin(-t+pi/2);
s.Zp = @(t) 0.8*sin(-t+pi/2) - 1i*1*cos(-t+pi/2);
s.Zpp = @(t) -0.8*cos(-t+pi/2) - 1i*1*sin(-t+pi/2);
param.p = 10; % p is the number of points in one panel
param.pnum_half = 30; 
param.N = param.pnum_half*param.p;
s.p = param.p;
s.tpan = 0:pi/param.pnum_half:pi;
[s, ~, ~] = quadrp(s, param.N, 'p', 'G');

% figure(1), plot(s.x), hold on, plot(s.x(1),'*r')
% axis equal, grid on
% quiver(real(s.x), imag(s.x), real(s.nx), imag(s.nx))
% quiver(real(s.x), imag(s.x), real(s.tang), imag(s.tang))


% generate a shape, ellipse, provide only s.Z
s2.Z = @(t) 0.8*cos(-t+pi/2) + 1i*1*sin(-t+pi/2);
s2.p = param.p;
s2.tpan = 0:pi/param.pnum_half:pi;
[s2, ~, ~] = quadrp(s2, param.N, 'p', 'G');

% generate a shape, ellipse, provide only s.Z, then given t_target,
% interpolate for x, xp, xpp, nx, tang, arclen
t_target = sort(pi*rand(100,1));    % in [0,pi]

x_ext = s.Z(t_target);
xp_ext = s.Zp(t_target);
xpp_ext = s.Zpp(t_target);
sp_ext = abs(xp_ext); 
tang_ext = xp_ext./sp_ext; 
nx_ext = -1i*tang_ext;

x_intp = interpolate_by_panel(s2, s.x, t_target);  % interpolation for x
xp_intp = interpolate_by_panel(s2, s.xp, t_target);  % interpolation for xp
xpp_intp = interpolate_by_panel(s2, s.xpp, t_target);  % interpolation for xpp
sp_intp = interpolate_by_panel(s2, s.sp, t_target);  % interpolation for sp
tang_intp = interpolate_by_panel(s2, s.tang, t_target);  % interpolation for unit tangent
nx_intp = interpolate_by_panel(s2, s.nx, t_target);   % interpolation for unit normal

figure, plot(t_target, dist_pointwise(x_intp,x_ext)), hold on
plot(t_target, dist_pointwise(xp_intp,xp_ext))
plot(t_target, dist_pointwise(xpp_intp,xpp_ext))
plot(t_target, dist_pointwise(sp_intp,sp_ext))
plot(t_target, dist_pointwise(tang_intp,tang_ext))
plot(t_target, dist_pointwise(nx_intp,nx_ext))


s2 = arclen_quadp(s2);

arclen_intp = interpolate_by_panel(s2, s2.arclen, t_target); 

fun = @(x) sqrt(0.64+0.36*((cos(pi/2-x)).^2));

arclen_ext = zeros(size(t_target));
for k=1:length(t_target)
arclen_ext(k) = integral(fun, 0, t_target(k));
end
plot(t_target, dist_pointwise(arclen_intp,arclen_ext))

plot(t_target, dist_pointwise(arclen_intp,arclen_ext))

legend('x error','xp error','xpp error','sp error','tang error','normal error','arclen error')


% 
s2 = arclen_quadp(s2);
arclen1 = s2.arclen;
s2 = arclen(s2);
arclen2 = s2.arclen;
figure, plot(s2.t, dist_pointwise(arclen1,arclen2))


