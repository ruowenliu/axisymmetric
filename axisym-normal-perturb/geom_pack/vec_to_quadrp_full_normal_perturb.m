function s = vec_to_quadrp_full_normal_perturb(xi_normal, param)
% This function computes (panel) quadrature
% input: xi_normal for \theta_n
% no restriction on xi_normal

theta_n.data = xi_normal;
theta_n.domain = 2*pi;
theta_n = geometryBSP_prd(xi_normal);

gz.data = z_full;
gz.domain = pi;
gz = geometryBSP_prd(gz);

s_circ.Z = @(t) gx.func(t)+1i*gz.func(t);
s_circ.p = param.p;
[s_circ,~] = quadrp(s_circ, param.N, 'p', 'G'); 
s = half_quadr(s_circ);
s = arclen(s);
end

