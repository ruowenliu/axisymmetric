% B-spline representation

% given cval for c_{0},...,c_{m}, corresponding to -5, ..., (MG-1)

% given t, the input

% given N, equals MG

% given dom = pi;

close all

MG = 10;
t = (-pi:0.001:2*pi)';
m = MG+4;
cval = 0.1*randn(m+1,1);

pval = Bsp5_scale_shift(t, (-5):(MG-1), MG, pi)*cval;

% Bsp5_scale_shift(t, (-5):(MG-1), MG, pi) is invariant for fix t (always, s.t doesn't change)

plot(t,pval)
axis([-pi,2*pi,-1,1])

