function [s, gx, gz] = vec_to_quadrp_full(vec, param)
% This function computes the (panel) quadrature, given variable vector

x = [real(param.xtop);vec(1:(param.shapeDoF-2));real(param.xbot)];
% z = [imag(param.xtop);vec((param.shapeDoF-1):end)];
z = vec((param.shapeDoF-1):end);
% generate x,z for the entire body.
x_full = [x; -flip(x(1:end-1))];
z_full = [z;flip(z(1:end-1))];

gx.data = x_full;
gx.domain = 2*pi;
gx = geometryBSP_prd(gx);

gz.data = z_full;
gz.domain = 2*pi;
gz = geometryBSP_prd(gz);

s_circ.Z = @(t) gx.func(t)+1i*gz.func(t);
s_circ.p = param.p;
[s_circ,~] = quadrp(s_circ, param.N, 'p', 'G'); 
s = half_quadr(s_circ);
s = arclen(s);
end