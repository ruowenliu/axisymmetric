function perturb_val = normal_perturbation(t,MG,xi_geom,eta_step,nx)


Mat_B_spline = Bsp5_scale_shift(t, (-5):(MG-1), MG, pi);
normal_perturb_half = Mat_B_spline*xi_geom;
normal_perturb = [normal_perturb_half; flip(normal_perturb_half)];
% eta_step = 0.5;
perturb_val = eta_step * normal_perturb .* nx;

end