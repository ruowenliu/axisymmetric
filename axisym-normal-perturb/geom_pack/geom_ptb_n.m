function snew = geom_ptb_n(xi_reduce, param)
% calculate new shape with perturbation along n, but fix z'(0)=z'(pi)=0
% s_base: old shape
% xi_reduce: coefficients for perturbation along n_perturb

% persistent phi_0 phi_pi phip_0 phip_pi
global s_base n_perturb

% MG = param.shapeDoF-5;
% 
% n3_0 = imag(interpolate_by_panel(s_base, n_perturb, 0));
% n3_pi = imag(interpolate_by_panel(s_base, n_perturb, pi));
% nxp = Deriv_panel(n_perturb,s_base);
% n3p_0 = imag(interpolate_by_panel(s_base, nxp, 0));
% n3p_pi = imag(interpolate_by_panel(s_base, nxp, pi));
% 
% n1_0 = real(interpolate_by_panel(s_base, n_perturb, 0));
% n1_pi = real(interpolate_by_panel(s_base, n_perturb, pi));
% 
% if isempty(phi_0)
%     phi_0 = Bsp5_scale_shift(0, (-5):(MG-1), MG, pi)';
%     phi_pi = Bsp5_scale_shift(pi, (-5):(MG-1), MG, pi)';
%     phip_0 = Bsp5_scale_shift_deriv(0, (-5):(MG-1), MG, pi)';
%     phip_pi = Bsp5_scale_shift_deriv(pi, (-5):(MG-1), MG, pi)';
% end

% xi_geom_hat_0 = zeros(param.shapeDoF,1);
% xi_geom_hat_m = zeros(param.shapeDoF,1);
% for id = 2:(param.shapeDoF-1)
%     xi_geom_hat = zeros(param.shapeDoF,1);
%     xi_geom_hat(id) = 1;
%     % solve for xi_geom_hat_0 and xi_geom_hat_m
%     RightVec = -[xi_geom_hat(id)*(phip_0(id)*n3_0+phi_0(id)*n3p_0); xi_geom_hat(id)*(phip_pi(id)*n3_pi+phi_pi(id)*n3p_pi)];
%     LeftMat = [phip_0(1)*n3_0+phi_0(1)*n3p_0, phip_0(end)*n3_0+phi_0(end)*n3p_0; phip_pi(1)*n3_pi+phi_pi(1)*n3p_pi, phip_pi(end)*n3_pi+phi_pi(end)*n3p_pi];
%     xi_geom_hat_0m = LeftMat\RightVec;   
%     xi_geom_hat_0(id) = xi_geom_hat_0m(1);
%     xi_geom_hat_m(id) = xi_geom_hat_0m(2);
% end
% 
% xi_geom = [0;xi_reduce;0];

% R1 = - interpolate_by_panel(s_base, imag(s_base.xp), 0) - xi_reduce'*phip_0(2:end-1)*n3_0 - xi_reduce'*phi_0(2:end-1)*n3p_0;
% R2 = - interpolate_by_panel(s_base, imag(s_base.xp), pi) - xi_reduce'*phip_pi(2:end-1)*n3_pi - xi_reduce'*phi_pi(2:end-1)*n3p_pi; 
% a = phip_0(1)*n3_0+phi_0(1)*n3p_0;
% b = phip_0(end)*n3_0+phi_0(end)*n3p_0;
% c = phip_pi(1)*n3_pi+phi_pi(1)*n3p_pi;
% d = phip_pi(end)*n3_pi+phi_pi(end)*n3p_pi;

xi_geom = [0;xi_reduce;0];
% xi_geom(1) = (d*R1-b*R2)/(a*d-b*c);
myfun = @(x) xi_find_1(x, xi_reduce, param);
xi_geom(1) = fzero(myfun, 0);
% xi_geom(end) = (-c*R1+a*R2)/(a*d-b*c);
myfun = @(x) xi_find_end(x, xi_reduce, param);
xi_geom(end) = fzero(myfun, 0);
% disp(xi_geom(end));

% R1 = - interpolate_by_panel(s_base, imag(s_base.xp), 0) - xi_reduce'*phip_0(3:end-2)*n3_0 - xi_reduce'*phi_0(3:end-2)*n3p_0;
% R2 = - interpolate_by_panel(s_base, imag(s_base.xp), pi) - xi_reduce'*phip_pi(3:end-2)*n3_pi - xi_reduce'*phi_pi(3:end-2)*n3p_pi; 
% R3 = - interpolate_by_panel(s_base, real(s_base.x), 0) - xi_reduce'*phi_0(3:end-2)*n1_0;
% R4 = - interpolate_by_panel(s_base, real(s_base.x), pi) - xi_reduce'*phi_pi(3:end-2)*n1_pi;
% a = zeros(4,4);
% a(1,1) = phip_0(1)*n3_0+phi_0(1)*n3p_0;
% a(1,2) = phip_0(2)*n3_0+phi_0(2)*n3p_0;
% a(1,3) = phip_0(end-1)*n3_0+phi_0(end-1)*n3p_0;
% a(1,4) = phip_0(end)*n3_0+phi_0(end)*n3p_0;
% a(2,1) = phip_pi(1)*n3_pi+phi_pi(1)*n3p_pi;
% a(2,2) = phip_pi(2)*n3_pi+phi_pi(2)*n3p_pi;
% a(2,3) = phip_pi(end-1)*n3_pi+phi_pi(end-1)*n3p_pi;
% a(2,4) = phip_pi(end)*n3_pi+phi_pi(end)*n3p_pi;
% a(3,1) = phi_0(1)*n1_0;
% a(3,2) = phi_0(2)*n1_0;
% a(3,3) = phi_0(end-1)*n1_0;
% a(3,4) = phi_0(end)*n1_0;
% a(4,1) = phi_pi(1)*n1_pi;
% a(4,2) = phi_pi(2)*n1_pi;
% a(4,3) = phi_pi(end-1)*n1_pi;
% a(4,4) = phi_pi(end)*n1_pi;
% xi_endsvec = a\[R1;R2;R3;R4];
% xi_geom = [xi_endsvec(1);xi_endsvec(2);xi_reduce;xi_endsvec(end-1);xi_endsvec(end)];

snew.x = s_base.x + param.MatT * xi_geom .* n_perturb;
snew.p = s_base.p; snew.tpan = s_base.tpan; snew.tlo = s_base.tlo; snew.thi = s_base.thi; snew.np = s_base.np;
snew = quadrp_givenx(snew, param.N, 'p', 'G');
snew = arclen_quadp(snew);


end