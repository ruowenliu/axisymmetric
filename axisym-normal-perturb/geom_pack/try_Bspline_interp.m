% try B-spline interpolation with half abscissae

m=20;
u=5:1:m;
un = [u(1); 0.5*(u(1)+u(2)); u(2); 0.5*(u(2)+u(3)); u(3:(end-2))'; 0.5*(u(end-2)+u(end-1)); u(end-1); 0.5*(u(end-1)+u(end)); u(end)];
startpoint = 1;
tval = (un-(5-startpoint))*(2*pi/(length(u)-1));
% val = sin(tval);
val = log(tval);
figure,plot(un,val,'o-')
A = zeros(m,m);
row1 = [1,26,66,26,1]/120;
row2 = [1,237,1682,1682,237,1]/3840;
A(1,1:5) = row1;
A(2,1:6) = row2;
A(3,2:6) = row1;
A(4,2:7) = row2;
A(end-3,(m-6):(m-1)) = row2;
A(end-2,(m-5):(m-1)) = row1;
A(end-1,(m-5):m) = row2;
A(end,(m-4):m) = row1;
for k=5:m-4
    A(k,k-2:k+2) = row1;
end

cval = A\val;

t=(1:0.01:2*pi)'; t2 = t/(2*pi)*(length(u)-1)+(5-startpoint); t3 = t2*2*pi/(length(u)-1);
interpval = Bsp5_scale_shift(t3, 0:(m-1), length(u)-1, 2*pi)*cval;


figure,plot(t,interpval,'ro');

% hold on, plot(t, sin(t),'b')
% max(abs(interpval - sin(t)))

hold on, plot(t, log(t),'b')
max(abs(interpval - log(t)))
