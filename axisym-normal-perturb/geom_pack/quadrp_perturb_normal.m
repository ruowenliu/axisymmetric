function s_new = quadrp_perturb_normal(s_old, xi_geom, param)
% generate quadrature given normal perturbation coeff xi_geom

MG = param.shapeDoF-5;
Mat_B_spline = Bsp5_scale_shift(s_old.t, (-5):(MG-1), MG, pi);
normal_perturb = Mat_B_spline*xi_geom;
s_new.x = s_old.x + normal_perturb .* s_old.nx;

s_new.p = s_old.p; s_new.tpan = s_old.tpan; s_new.tlo = s_old.tlo; s_new.thi = s_old.thi; s_new.np = s_old.np;
s_new = quadrp_givenx(s_new, param.N, 'p', 'G');
s_new = arclen_quadp(s_new);

end