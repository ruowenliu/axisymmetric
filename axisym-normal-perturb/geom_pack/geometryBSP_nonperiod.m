function obj = geometryBSP_nonperiod(obj)
% This function interpolate periodic function by 5th order Bspline:
% (1) Given a data array "data" for 0:N, N is a positive integer,
%     compute the Bspline interpolation function
%     for a scaled domain [0,domain]
%     data - must be a column vector
% (2) (incomplete) Given a coefficient array "cf",
%     compute the Bspline interpolation function
%
% obj = struct;
% obj.data = data;
% obj.cf = cf;
% obj.N = N;
% obj.domain = domain;
% obj.func = func;
%
% version: 08/10/2020
% Ruowen Liu, ruowen@umich.edu
persistent Amat

if isfield(obj, 'data')
    % use data to compute func
    obj.N = length(obj.data)-1;
    
    %-------------------------------
    % The following solves the following problem
    % s(t) = c_{-5} * B5(t+5) + ... + c_{N-1} * B5(t-(N-1))
    % s(0), ..., s(N) are given in the array s
    
    if size(obj.data,2)~=1
        s = obj.data';
    else
        s = obj.data;
    end
    N = numel(s)-1;
    if isempty(Amat)
        Amat = zeros(N+5,N+5);
        Amat(1,1:5) = [-1,-10,0,10,1]/24;
        Amat(1,end-4:end) = -Amat(1,1:5);
        Amat(2,1:5) = [1,2,-1,2,1]/6;
        Amat(2,end-4:end) = -Amat(2,1:5);
        Amat(3,1:5) = [-0.5,1,0,-1,0.5];
        Amat(3,end-4:end) = -Amat(3,1:5);
        Amat(4,1:5) = [1,-4,6,-4,1];
        Amat(4,end-4:end) = -Amat(4,1:5);
        for k=5:N+5
            Amat(k,k-4:k) = [1,26,66,26,1]/120;
        end
    end
    c = Amat\[0;0;0;0;s];
    %-------------------------------
    %     c = solveBSP(obj.data);
    obj.func = @(t) BSPintp_prd(t*obj.N/obj.domain, c, obj.N);
end

if isfield(obj, 'cf')
    % use cf to compute func
    warning('incomplete.')
end
end

function B = BSPintp_prd(t, c, N)
sizet = size(t);
[t,~] = shiftinrange(t,N);
if sizet(2)~=1
    t = reshape(t,numel(t),1);
end
d = numel(c); % number of splines or coefficents
k = 5;   % start at B5(t+5)
B = Bspline5(t+k-((1:d)-1))*c;
if sizet(2)~=1
    B = reshape(B,sizet(1),sizet(2));
end
end

function [t, shiftcount] = shiftinrange(t, N)
% shift range if t is not in [0,N]
shiftcount = zeros(size(t));
while ~min(min(t>=0 & t<=N))
    while max(max(t<0))
        shiftcount = shiftcount - (t<0);
        t = t+N*(t<0);
    end
    while max(max(t>N))
        shiftcount = shiftcount + (t>N);
        t = t-N*(t>N);
    end
end
end

function B = Bspline5(t)
% fifth order B-spline function, no shift
p1 = t.^5/120;
p2 = (t.^4.*(2-t)+t.^3.*(3-t).*(t-1)+t.^2.*(4-t).*(t-1).^2+...
    t.*(5-t).*(t-1).^3+(6-t).*(t-1).^4)/120;
p3 = (t.^3.*(3-t).^2+t.^2.*(4-t).*(t-1).*(3-t)+t.^2.*(4-t).^2.*(t-2)+...
    t.*(5-t).*(t-1).^2.*(3-t)+t.*(5-t).*(t-1).*(4-t).*(t-2)+...
    t.*(5-t).^2.*(t-2).^2+(6-t).*(t-1).^3.*(3-t)+(6-t).*(t-1).^2.*(4-t).*(t-2)+...
    (6-t).*(t-1).*(5-t).*(t-2).^2+(6-t).^2.*(t-2).^3)/120;
p4 = (t.^2.*(4-t).^3+t.*(5-t).*(t-1).*(4-t).^2+t.*(5-t).^2.*(t-2).*(4-t)+...
    t.*(5-t).^3.*(t-3)+(6-t).*(t-1).*(5-t).*(t-2).*(4-t)+...
    (6-t).*(t-1).^2.*(4-t).^2+(6-t).*(t-1).*(5-t).^2.*(t-3)+...
    (6-t).^2.*(t-2).*(5-t).*(t-3)+(6-t).^2.*(t-2).^2.*(4-t)+(6-t).^3.*(t-3).^2)/120;
p5 = ((6-t).^4.*(t-4)+(6-t).^3.*(t-3).*(5-t)+(6-t).^2.*(t-2).*(5-t).^2+...
    (6-t).*(t-1).*(5-t).^3+t.*(5-t).^4)/120;
p6 = ((6-t).^5)/120;
B = p1.*(t>=0 & t<1)+p2.*(t>=1 & t<2)+p3.*(t>=2 & t<3)+...
    p4.*(t>=3 & t<4)+p5.*(t>=4 & t<5)+p6.*(t>=5 & t<6);
end