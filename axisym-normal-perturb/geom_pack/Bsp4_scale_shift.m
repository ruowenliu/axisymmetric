function val = Bsp4_scale_shift(t, k, N, dom)
% fifth order B-spline function
% t -- independent variable
% k -- shift index
% N -- number of data
% dom -- domain for data

tval = t*N/dom - k;

p1 = tval.^4/24;
p2 = (tval.^3.*(2-tval)+tval.^2.*(3-tval).*(tval-1)+tval.*(4-tval).*(tval-1).^2+(5-tval).*(tval-1).^3)/24;
p3 = (tval.^2.*(3-tval).^2+tval.*(4-tval).*(tval-1).*(3-tval)+(5-tval).*(tval-1).*(4-tval).*(tval-2)+...
    (5-tval).^2.*(tval-2).^2+tval.*(4-tval).^2.*(tval-2)+(5-tval).*(tval-1).^2.*(3-tval))/24;
p4 = ((5-tval).^3.*(tval-3)+(5-tval).^2.*(tval-2).*(4-tval)+(5-tval).*(tval-1).*(4-tval).^2+tval.*(4-tval).^3)/24;
p5 = ((5-tval).^4)/24;
val = p1.*(tval>=0 & tval<1)+p2.*(tval>=1 & tval<2)+p3.*(tval>=2 & tval<3)+p4.*(tval>=3 & tval<4)+p5.*(tval>=4 & tval<5);
end