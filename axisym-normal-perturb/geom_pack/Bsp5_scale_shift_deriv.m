function valp = Bsp5_scale_shift_deriv(t, k, N, dom)
% fifth order B-spline function
% t -- independent variable
% k -- shift index
% N -- number of data
% dom -- domain for data

valp = Bsp4_scale_shift(t, k, N, dom) - Bsp4_scale_shift(t, k+1, N, dom);
end