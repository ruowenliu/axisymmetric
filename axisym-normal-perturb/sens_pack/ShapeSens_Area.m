function val = ShapeSens_Area(s,tht)
% Shape sensitivity of surface area

s.kap = - s.cur;
tht_n = dotv(tht,s.nx);
Z_deriv = Deriv_panel(imag(s.x),s)./s.sp;
val = 2*pi*s.ws'*( (Z_deriv - s.kap.*real(s.x)) .* tht_n);

end