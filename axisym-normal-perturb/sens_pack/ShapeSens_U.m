function sensU = ShapeSens_U(s, tht, param)
% this function computes the shape sensitivity of U
% given shape transformations velocity theta (tht)

s.kap = - s.cur;
tht_s = dotv(tht,s.tang);
tht_n = dotv(tht,s.nx);
fs    = dotv(s.trac, s.tang);
fshat = dotv(s.trachat, s.tang);

% Dtht_s = Deriv_panel(tht_s,s)./s.sp;

R = real(s.x);

Dus = Deriv_panel(s.uslip,s)./s.sp;

DRusthtn = Deriv_panel((R.*s.uslip.*tht_n),s)./s.sp;

F0 = 2*pi*s.ws'*(imag(s.trachat).*real(s.x));

integ = s.kap.*tht_n;
s_star = tht_s - integral_to_quadrp(s, integ);

el = sum(s.ws);
el_star = - s.ws'*(s.kap.*tht_n);

term1 = R.*fshat.*(s_star-tht_s-s.arclen/el*el_star).*Dus;

term2 = -s.preshat.*DRusthtn;

term3a = s.kap.*R.*s.uslip.*fshat;

term3b = -1/param.mu*R.*fs.*fshat;

term3 = (term3a+term3b).*tht_n;

sensU = -2*pi/F0*(s.ws'*(term1+term2+term3));

end