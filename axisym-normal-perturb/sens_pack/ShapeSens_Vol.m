function val = ShapeSens_Vol(s,tht)
% Shape sensitivity of volume

tht_n = dotv(tht,s.nx);
val = - 2*pi*s.ws'*(tht_n.*real(s.x));

end