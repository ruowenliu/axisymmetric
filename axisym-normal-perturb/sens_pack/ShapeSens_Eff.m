function sensEff = ShapeSens_Eff(s, tht, param)
% this function computes the shape sensitivity of efficiency
% given shape transformations velocity theta (tht)

sensJw = ShapeSens_Jw(s, tht, param);

sensU = ShapeSens_U(s, tht, param);

sensF0 = ShapeSens_F0(s, tht, param);

sensEff = ((sensF0*(s.U^2)+s.F0*2*s.U*sensU)*s.Jw - s.F0*(s.U^2)*sensJw)/(s.Jw^2);

end