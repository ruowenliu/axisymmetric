function sensJw = ShapeSens_Jw(s, tht, param)
% this function computes the shape sensitivity of Jw
% given shape transformations velocity theta (tht)

s.kap = - s.cur;
tht_s = dotv(tht,s.tang);
tht_n = dotv(tht,s.nx);
fs    = dotv(s.trac, s.tang);
fn    = dotv(s.trac, s.nx);

Dtht_n = Deriv_panel(tht_n,s)./s.sp;

R = real(s.x);
DR = Deriv_panel(R,s)./s.sp;

Dus = Deriv_panel(s.uslip,s)./s.sp;

DRus = Deriv_panel((R.*s.uslip),s)./s.sp;

integ = s.kap.*tht_n;
s_star = tht_s - integral_to_quadrp(s, integ);

el = sum(s.ws);
el_star = - s.ws'*(s.kap.*tht_n);

term1 = R.*fs.*(s_star-tht_s-s.arclen/el*el_star).*Dus;

term2 = s.uslip.*fn.*Dtht_n.*R;

term3a = -2*param.mu*Dus.*DR.*s.uslip;

term3b = s.kap.*R.*s.uslip.*fs;

term3c = -.5/param.mu*R.*(fs.^2);

term3d = -s.pres.*DRus;

term3 = (term3a+term3b+term3c+term3d).*tht_n;

sensJw = 4*pi*(s.ws'*(term1+term2+term3));

end