function sensF0 = ShapeSens_F0(s, tht, param)
% this function computes the shape sensitivity of F0 (drag coefficient)
% given shape transformations velocity theta (tht)

fshat = dotv(s.trachat, s.tang);
tht_n = dotv(tht,s.nx);
sensF0 = -1/param.mu*2*pi*s.ws'*(fshat.*fshat.*tht_n.*real(s.x));

end