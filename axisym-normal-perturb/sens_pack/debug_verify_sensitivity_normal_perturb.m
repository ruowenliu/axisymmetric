% verify sensitivity

close all
clear
clear global
clear geometryBSP_prd
clear uslipBSP_prd
clear plot_bodyshape
format long
format compact

%%
% tic
param.mu = 1;
param.slipDoF = 15; % degree of freedom for uslip

param.p = 10; % p is the number of points in one panel
param.panel_num = 30*2; % the first number is the default num of panels for the generating arc (top to bottom)
param.N = param.panel_num*param.p;
param.xtop = 0; param.xbot = 0;  % must equal zero

param.geomDoF = 11;  % This number is degree of freedom for theta_n
r1 = 0.8;
% r1 = 1;
r2 = 1;
x = r1*cos(pi/2-linspace(0,pi,param.shapeDoF))';
z = r2*sin(pi/2-linspace(0,pi,param.shapeDoF))';
xi_geom = [x(2:end-1);z(1:end)]; % length: shapeNc*2-2
% rng(1)
% xi_geom = xi_geom + 0.04*randn(size(xi_geom));

[s, ~, ~] = vec_to_quadrp_full(xi_geom, param);

%%
figure(1), plot_bodyshape(s,[],[1,0,0]), title('test (initial) shape')
%%

grad_data = zeros(length(xi_geom),4);

tic
[maxeig_val,xi_slip,~,eigenD,LAMBDA_val] = find_max_eigenvalue(s, param);
toc

[~,ind]= max(abs(diag(eigenD)));

fprintf('error for the max eigenvalue = %g \n', eigenD(ind,ind) - maxeig_val)

%%

% prescribe the optimal slip
% usfunc_uni_arc = @(s) sin(pi*s)+0.1*sin(6*pi*s);
% s.uslip = usfunc_uni_arc(s.arclen/s.ell);
controlpoint = [0;xi_slip;0; -flip(xi_slip);0];
usfunc = uslipBSP_prd(controlpoint);
s.uslip = usfunc(s.t);    
figure, plot(s.arclen/s.ell, s.uslip), title('U^s (s/l)')


%%
clear forwardsolve_nnf
s = forwardsolve_nnf(s,'forward','adjoint','nobasis');
%

%%
xi_reduce = zeros(param.shapeDoFmod,1);

global s_base n_perturb
s_base = s; n_perturb = s.nx;

%%
hs = 1e-4;
testcase = 8; % 1: volume, 2: area, 3: Jw, 4: U, 5: F0, 6: JD, 7: efficiency, 8: max_eigenvalue
disp(testcase);

for k = [1:3,length(xi_reduce)-2:length(xi_reduce)]
    
    tic 
    
    xi_reduce_perturb = xi_reduce; 
    xi_reduce_perturb(k) = xi_reduce(k) + 1;
    s_perturb = geom_ptb_n(xi_reduce_perturb, param);
    tht = s_perturb.x - s.x;
    
    xi_reduce_new1 = xi_reduce; xi_reduce_new1(k) = xi_reduce(k) + hs;
    s_new1 = geom_ptb_n(xi_reduce_new1, param);
    sl_q = [0;s.arclen/s.ell;1]; s_new1.uslip = interp1(sl_q, [0;s.uslip;0], s_new1.arclen/s_new1.ell);
    clear forwardsolve_nnf
    s_new1 = forwardsolve_nnf(s_new1,'forward','adjoint','nobasis');
    
    xi_reduce_new2 = xi_reduce; xi_reduce_new2(k) = xi_reduce(k) - hs;
    s_new2 = geom_ptb_n(xi_reduce_new2, param);
    sl_q = [0;s.arclen/s.ell;1]; s_new2.uslip = interp1(sl_q, [0;s.uslip;0], s_new2.arclen/s_new2.ell);
    clear forwardsolve_nnf
    s_new2 = forwardsolve_nnf(s_new2,'forward','adjoint','nobasis');
    
    switch testcase
        case 1
            grad_star = ShapeSens_Vol(s, tht);
            val_1 = s_new1.vol;
            val_2 = s_new2.vol;
            grad_fd = (val_1 - val_2)/2/hs;
        case 2
            grad_star = ShapeSens_Area(s, tht);
            val_1 = s_new1.area;
            val_2 = s_new2.area;
            grad_fd = (val_1 - val_2)/2/hs;
        case 3
            grad_star = ShapeSens_Jw(s, tht, param);
            val_1 = s_new1.Jw;
            val_2 = s_new2.Jw;
            grad_fd = (val_1 - val_2)/2/hs;
        case 4
            grad_star = ShapeSens_U(s, tht, param);
            val_1 = s_new1.U;
            val_2 = s_new2.U;
            grad_fd = (val_1 - val_2)/2/hs;
        case 5
            grad_star = ShapeSens_F0(s, tht, param);
            val_1 = s_new1.F0;
            val_2 = s_new2.F0;
            grad_fd = (val_1 - val_2)/2/hs;
        case 6
            grad_star = ShapeSens_JD(s, tht, param);
            val_1 = s_new1.JD;
            val_2 = s_new2.JD;
            grad_fd = (val_1 - val_2)/2/hs;
        case 7
            grad_star = ShapeSens_Eff(s, tht, param);
            val_1 = s_new1.eff;
            val_2 = s_new2.eff;
            grad_fd = (val_1 - val_2)/2/hs;
        case 8
            grad_star = ShapeSens_LAMBDA(s, tht, param, s.LAMBDA, s.xi_slip);
            [~, val_1] = find_optimal_slip(s_new1, param);
            [~, val_2] = find_optimal_slip(s_new2, param);
            grad_fd = (val_1 - val_2)/2/hs;
            
    end
   
    grad_data(k,1) = grad_star;
    grad_data(k,2) = grad_fd;
    grad_data(k,3) = abs(grad_star-grad_fd);
    grad_data(k,4) = abs(grad_star-grad_fd)./abs(grad_fd)*100;
        
    fprintf('k = %i\n',k)
    fprintf('grad_star is %g\n', grad_star);
    fprintf('grad_fd is %g\n', grad_fd);
    fprintf('abs error %e\n', abs(grad_star-grad_fd));
    fprintf('rel error %e %%\n', abs(grad_star-grad_fd)./abs(grad_fd)*100);
    
    toc
    
end

figure, semilogy(grad_data(:,3),'.-'), title('absolute error')
figure, plot(grad_data(:,4),'.-'), title('relative error (%)')
    

%%
for k = 1:length(xi_geom)
    
    tic 
    
    xi_geom_perturb = xi_geom; 
    xi_geom_perturb(k) = xi_geom(k) + 1;
    [s_perturb, ~, ~] = vec_to_quadrp_full(xi_geom_perturb, param);
    tht = s_perturb.x - s.x;
    
    xi_geom_new1 = xi_geom; xi_geom_new1(k) = xi_geom(k) + hs;
    [s_new1, ~, ~] = vec_to_quadrp_full(xi_geom_new1, param);
    s_new1 = arclen(s_new1);
    
    sl_q = [0;s.arclen/s.ell;1]; s_new1.uslip = interp1(sl_q, [0;s.uslip;0], s_new1.arclen/s_new1.ell);
%     s_new1.uslip = usfunc_uni_arc(s_new1.arclen/s_new1.ell);
    clear forwardsolve_nnf
    s_new1 = forwardsolve_nnf(s_new1,'forward','adjoint','nobasis');
    
    xi_geom_new2 = xi_geom; xi_geom_new2(k) = xi_geom(k) - hs;
    [s_new2, ~, ~] = vec_to_quadrp_full(xi_geom_new2, param);
    s_new2 = arclen(s_new2);
    s_new2.uslip = interp1(sl_q, [0;s.uslip;0], s_new2.arclen/s_new2.ell);
%     s_new2.uslip = usfunc_uni_arc(s_new2.arclen/s_new2.ell);
    clear forwardsolve_nnf
    s_new2 = forwardsolve_nnf(s_new2,'forward','adjoint','nobasis');
    
    switch testcase
        case 1
            grad_star = ShapeSens_Vol(s, tht);
            val_1 = s_new1.vol;
            val_2 = s_new2.vol;
            grad_fd = (val_1 - val_2)/2/hs;
        case 2
            grad_star = ShapeSens_Area(s, tht);
            val_1 = s_new1.area;
            val_2 = s_new2.area;
            grad_fd = (val_1 - val_2)/2/hs;
        case 3
            grad_star = ShapeSens_Jw(s, tht, param);
            val_1 = s_new1.Jw;
            val_2 = s_new2.Jw;
            grad_fd = (val_1 - val_2)/2/hs;
        case 4
            grad_star = ShapeSens_U(s, tht, param);
            val_1 = s_new1.U;
            val_2 = s_new2.U;
            grad_fd = (val_1 - val_2)/2/hs;
        case 5
            grad_star = ShapeSens_F0(s, tht, param);
            val_1 = s_new1.F0;
            val_2 = s_new2.F0;
            grad_fd = (val_1 - val_2)/2/hs;
        case 6
            grad_star = ShapeSens_JD(s, tht, param);
            val_1 = s_new1.JD;
            val_2 = s_new2.JD;
            grad_fd = (val_1 - val_2)/2/hs;
        case 7
            grad_star = ShapeSens_Eff(s, tht, param);
            val_1 = s_new1.eff;
            val_2 = s_new2.eff;
            grad_fd = (val_1 - val_2)/2/hs;
        case 8
            grad_star = ShapeSens_LAMBDA(s, tht, param, LAMBDA_val, xi_slip);
            [~,~,~,eigenD_1, val_1] = find_max_eigenvalue(s_new1, param);
            [~,ind_1]= max(abs(diag(eigenD_1)));fprintf('error for the max eigenvalue (perturbed 1) = %g \n',eigenD_1(ind_1,ind_1) - val_1);
            [~,~,~,eigenD_2, val_2] = find_max_eigenvalue(s_new2, param);
            [~,ind_2]= max(abs(diag(eigenD_2)));fprintf('error for the max eigenvalue (perturbed 2) = %g \n', eigenD_2(ind_2,ind_2) - val_2)
            grad_fd = (val_1 - val_2)/2/hs;
    end
    
    toc
   
    grad_data(k,1) = grad_star;
    grad_data(k,2) = grad_fd;
    grad_data(k,3) = abs(grad_star-grad_fd);
    grad_data(k,4) = abs(grad_star-grad_fd)./abs(grad_fd)*100;
        
    fprintf('k = %i\n',k)
    fprintf('grad_star is %g\n', grad_star);
    fprintf('grad_fd is %g\n', grad_fd);
    fprintf('abs error %e\n', abs(grad_star-grad_fd));
    fprintf('rel error %e %%\n', abs(grad_star-grad_fd)./abs(grad_fd)*100);
    
end

figure, semilogy(grad_data(:,3),'.-'), title('absolute error')
figure, plot(grad_data(:,4),'.-'), title('relative error (%)')

% toc

