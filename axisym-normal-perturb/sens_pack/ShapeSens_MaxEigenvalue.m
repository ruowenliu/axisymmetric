function sensME = ShapeSens_MaxEigenvalue(s, tht, param, max_eigenvalue, xi_slip)
% this function computes the shape sensitivity of max eigenvalue
% given shape transformations velocity theta (tht)

if nargin == 3
[max_eigenvalue, xi_slip,~,~] = find_max_eigenvalue(s, param);
end

controlpoint = [0;xi_slip;0; -flip(xi_slip);0];
usfunc = uslipBSP_prd(controlpoint);
s.uslip = usfunc(s.t);    
s = forwardsolve_nnf(s);

sensJw = ShapeSens_Jw(s, tht, param);
sensJD = ShapeSens_JD(s, tht, param);

sensME = (sensJD - max_eigenvalue*sensJw)/s.Jw;

end