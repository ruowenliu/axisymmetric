function sensJD = ShapeSens_JD(s, tht, param)
% this function computes the shape sensitivity of JD
% given shape transformations velocity theta (tht)

sensF0 = ShapeSens_F0(s, tht, param);
sensU = ShapeSens_U(s, tht, param);
sensJD = sensF0*(s.U^2) + 2*s.F0*s.U*sensU;

end