function sensME = ShapeSens_LAMBDA(s, tht, param, LAMBDA, xi_slip)
% this function computes the shape sensitivity of max eigenvalue
% given shape transformations velocity theta (tht)

if nargin == 3
[~, xi_slip,~,~,LAMBDA] = find_max_eigenvalue(s, param);
end

if ~isfield(s, 'Jw')
controlpoint = [0;xi_slip;0; -flip(xi_slip);0];
usfunc = uslipBSP_prd(controlpoint);
s.uslip = usfunc(s.t);    
s = forwardsolve_nnf(s,'forward','adjoint','nobasis');
end

sensJw = ShapeSens_Jw(s, tht, param);
sensJD = ShapeSens_JD(s, tht, param);

sensME = (sensJD - LAMBDA*sensJw)/s.Jw;

end