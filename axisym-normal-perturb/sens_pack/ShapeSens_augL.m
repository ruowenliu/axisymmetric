function sensAL = ShapeSens_augL(s, tht, param, LAMBDA, xi_slip, lam, sig, C_vol, C_area)
% this function computes the shape sensitivity of augmented Lagrangian
% given shape transformations velocity theta (tht)

% global iterations
% lam = iterations.lam(end,:);
% sig = iterations.sig(end);
% 
% C_vol = s.vol-param.vol_target;
% C_area = s.area-param.area_target;

if ~isfield(s, 'eff')
    
if ~isfield(s, 'LAMBDA')
[xi_slip, LAMBDA] = find_optimal_slip(s, param);
end

controlpoint = [0;xi_slip;0; -flip(xi_slip);0];
usfunc = uslipBSP_prd(controlpoint);
s.uslip = usfunc(s.t);
clear forwardsolve_nnf    %%% added on Feb.4.2021
s = forwardsolve_nnf(s,'forward','adjoint','nobasis');

end

sensJw = ShapeSens_Jw(s, tht, param);
sensJD = ShapeSens_JD(s, tht, param);

sensLAMBDA = (sensJD - LAMBDA*sensJw)/s.Jw;

sens_vol = ShapeSens_Vol(s, tht);

sens_area = ShapeSens_Area(s, tht);

sensAL = -sensLAMBDA - lam(1)*sens_vol - lam(2)*sens_area ...
    + sig*(C_vol*sens_vol + C_area*sens_area);

end