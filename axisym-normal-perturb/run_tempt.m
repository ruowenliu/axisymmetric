global iterations Solver s_base n_perturb

while stop_opt == 0  
if ~isfield(iterations, 'ALMnum')   % initialize Augmented Lagrangian param
    iterations.ALMnum = 1;
    iterations.stepsize = 0;
    % prepare for optimization
    iterations.lam = [0,0];
    if ~isfield(iterations, 'sig')
        iterations.sig = 20;
    end
    iterations.constraints_tol = 1./(iterations.sig(end).^0.1); % tolerance for constraints
    iterations.convergence_tol = 0.001;   % convergence tolerance
    % tab is used to show update whether multiplier or penalty
    iterations.tab = {'I'}; % initial
else
    maxconstval = max([abs(Solver.C_vol),abs(Solver.C_area)]);
    if maxconstval < iterations.constraints_tol(end)
        if maxconstval < iterations.convergence_tol(end)
            fprintf('---\nOptimization is completed because the constraints are satisfied.\n---\n')
            stop_opt = 1;
        else
            % Update multipliers, tighten tolerances            
            iterations.ALMnum = [iterations.ALMnum; iterations.ALMnum(end)+1];
            iterations.stepsize = [iterations.stepsize; 0];
            iterations.convergence_tol = [iterations.convergence_tol; iterations.convergence_tol(end)];
            iterations.lam = [iterations.lam; NaN, NaN];
            iterations.lam(end,1) = iterations.lam(end-1,1)-iterations.sig(end).*Solver.C_vol;
            iterations.lam(end,2) = iterations.lam(end-1,2)-iterations.sig(end).*Solver.C_area;
            iterations.constraints_tol = [iterations.constraints_tol; NaN];
            iterations.constraints_tol(end) = iterations.constraints_tol(end-1)./(iterations.sig(end)^0.9);
            iterations.tab = [iterations.tab; 'M']; % update multiplier
            iterations.sig = [iterations.sig; iterations.sig(end)]; % keep same penalty
        end
    else
        % Increase penalty parameter, tighten tolerances
        iterations.ALMnum = [iterations.ALMnum; iterations.ALMnum(end)+1];
        iterations.stepsize = [iterations.stepsize; 0];
        iterations.convergence_tol = [iterations.convergence_tol; iterations.convergence_tol(end)];
        iterations.sig = [iterations.sig; NaN];
        iterations.sig(end) = 10*iterations.sig(end-1);
        iterations.constraints_tol = [iterations.constraints_tol; NaN];
        iterations.constraints_tol(end) = 1./(iterations.sig(end).^0.1);
        iterations.tab = [iterations.tab; 'P'];  % update penalty
        iterations.lam = [iterations.lam; iterations.lam(end,:)]; % keep same multiplier
    end    
end

if stop_opt == 0    
    % --------------- find max eff by fminunc --------------- 
    func = @(x) obj_LAMBDAmod(x,param);
    
    while flag == 1
    [xi_reduce,fval,exitflag,output] = fminunc(func,xi_reduce,options);
    % --- update n_perturb ---
%     n_perturb = Solver.nx;
    s_base = Solver;
    xi_reduce = zeros(param.shapeDoFmod,1);
    end
    n_perturb = Solver.nx;
    flag = 1;
%     while Solver.relocate == 1
%         xi_geom = relocate_vec(xi_geom,param);
%         [xi_geom,fval,exitflag,output] = fminunc(func,xi_geom,options);
%     end
%         
%     if iterations.gradient_max(end) > 1e8
%         fprintf('--\ngradient too large.\n--\n')
%         %         keyboard
%     end
    if loop_counter >= 8
        stop_opt = -1;
        fprintf('--\n Warning: loop_counter too large. \n--\n')
        %         keyboard
    end
end

end