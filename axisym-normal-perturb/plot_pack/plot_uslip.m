function plot_uslip(s)

plot(s.arclen/s.ell, s.uslip,'linewidth',1.5, 'color',[89,136,107]/255);

set(gca,'fontsize',20)

hold on

% plot endpoints of panels
plot(s.arclen([1:s.p:end, end])/s.ell,s.uslip([1:s.p:end, end]),'*','color',[89,136,107]/255,'linewidth',1.5);

xlabel('scaled arclength $s/ \ell$','interpreter','latex')
ylabel('$u_s$','interpreter','latex')
title('optimal slip velocity','interpreter','latex')
end