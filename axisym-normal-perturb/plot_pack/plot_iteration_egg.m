% plot iteration figures

load('./output/test_1216_5.mat')

%%
close all

vidfile = VideoWriter('shape_iter_eff_egg.mp4','MPEG-4');
vidfile.FrameRate = 2; % set speed
open(vidfile);

ITERnum = length(iterations.area);

EFF = [];
for k = 1:ITERnum
    figure(k)
    s=iterations.Solver(k);
    subplot(1,2,1)
    plot_bodyshape(s,[],[0,102/255,204/255])
    set(gca,'fontsize',15)
    title(['iter num = ' num2str(k-1)], 'interpreter','latex')
    axis([-1.2,1.2,-1.65,1.65])
    subplot(1,2,2)
    EFF = [EFF; s.eff];
    plot(0:k-1,EFF,'o-b','markerfacecolor','b','markersize',3);
    axis([-0.5,155,0.6,1.3])
    set(gca,'fontsize',15), grid on
    xlabel('iterations','interpreter','latex')
    ylabel('efficiency $F_0 U^2 / J_W$','interpreter','latex')
    F(k) = getframe(gcf);
    writeVideo(vidfile,F(k));
end

close(vidfile)

%%

close all

vidfile = VideoWriter('shape_iter_c_vol_1225.mp4','MPEG-4');
vidfile.FrameRate = 2; % set speed
open(vidfile);

ITERnum = length(iterations.area);

CVOL = [];
for k = 1:ITERnum
    figure(k)
    s=iterations.Solver(k);
    subplot(1,2,1)
    plot_bodyshape(s,[],[0,102/255,204/255])
    set(gca,'fontsize',15)
    title(['iter num = ' num2str(k-1)], 'interpreter','latex')
    axis([-1.2,1.2,-1.5,1.5])
    subplot(1,2,2)
    CVOL = [CVOL; s.vol-param.vol_target];
    plot(0:k-1,CVOL,'o-b','markerfacecolor','b','markersize',3);
%     axis([-0.5,50,-0.1,0.06])
    set(gca,'fontsize',15)
    xlabel('iterations','interpreter','latex')
    ylabel('volume constraint $V-V_{0}$','interpreter','latex')
    F(k) = getframe(gcf);
    writeVideo(vidfile,F(k));
end

close(vidfile)

%%

close all

vidfile = VideoWriter('shape_iter_c_area_1225.mp4','MPEG-4');
vidfile.FrameRate = 2; % set speed
open(vidfile);

ITERnum = length(iterations.area);

CAREA = [];
for k = 1:ITERnum
    figure(k)
    s=iterations.Solver(k);
    subplot(1,2,1)
    plot_bodyshape(s,[],[0,102/255,204/255])
    set(gca,'fontsize',15)
    title(['iter num = ' num2str(k-1)], 'interpreter','latex')
    axis([-1.2,1.2,-1.5,1.5])
    subplot(1,2,2)
    CAREA = [CAREA; s.area-param.area_target];
    plot(0:k-1,CAREA,'o-b','markerfacecolor','b','markersize',3);
%     axis([-0.5,50,-0.1,0.06])
    set(gca,'fontsize',15)
    xlabel('iterations','interpreter','latex')
    ylabel('surface area constraint $A-A_{0}$','interpreter','latex')
    F(k) = getframe(gcf);
    writeVideo(vidfile,F(k));
end

close(vidfile)

%%
close all