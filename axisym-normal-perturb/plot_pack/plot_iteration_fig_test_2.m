% plot iteration figures

load('./output/test_2_1AL.mat')

%%
close all

vidfile = VideoWriter('shape_iter_eff_test_2.mp4','MPEG-4');
vidfile.FrameRate = 2; % set speed
open(vidfile);

ITERnum = length(iterations.area);

EFF = [];
for k = 1:ITERnum
    figure(k)
    s=iterations.Solver(k).s;
    subplot(1,2,1)
    plot_bodyshape(s,[],[0,102/255,204/255])
    set(gca,'fontsize',15)
    title(['iter num = ' num2str(k-1)], 'interpreter','latex')
    axis([-1.2,1.2,-1.5,1.5])
    subplot(1,2,2)
    EFF = [EFF; s.eff];
    plot(0:k-1,EFF,'o-b','markerfacecolor','b','markersize',3);
    axis([-0.5,15,0.8,1.5])
    set(gca,'fontsize',15)
    xlabel('iterations','interpreter','latex')
    ylabel('efficiency $F_0 U^2 / J_W$','interpreter','latex')
    F(k) = getframe(gcf);
    writeVideo(vidfile,F(k));
end

close(vidfile)

%%

close all

vidfile = VideoWriter('shape_iter_c_vol_test_2.mp4','MPEG-4');
vidfile.FrameRate = 2; % set speed
open(vidfile);

ITERnum = length(iterations.area);

CVOL = [];
for k = 1:ITERnum
    figure(k)
    s=iterations.Solver(k).s;
    subplot(1,2,1)
    plot_bodyshape(s,[],[0,102/255,204/255])
    set(gca,'fontsize',15)
    title(['iter num = ' num2str(k-1)], 'interpreter','latex')
    axis([-1.2,1.2,-1.5,1.5])
    subplot(1,2,2)
    CVOL = [CVOL; s.vol-param.vol_target];
    plot(0:k-1,CVOL,'o-b','markerfacecolor','b','markersize',3);
    axis([-0.5,15,-0.18,0.0])
    set(gca,'fontsize',15)
    xlabel('iterations','interpreter','latex')
    ylabel('volume constraint $V-V_{0}$','interpreter','latex')
    F(k) = getframe(gcf);
    writeVideo(vidfile,F(k));
end

close(vidfile)

%%

close all

vidfile = VideoWriter('shape_iter_c_area_test_2.mp4','MPEG-4');
vidfile.FrameRate = 2; % set speed
open(vidfile);

ITERnum = length(iterations.area);

CAREA = [];
for k = 1:ITERnum
    figure(k)
    s=iterations.Solver(k).s;
    subplot(1,2,1)
    plot_bodyshape(s,[],[0,102/255,204/255])
    set(gca,'fontsize',15)
    title(['iter num = ' num2str(k-1)], 'interpreter','latex')
    axis([-1.2,1.2,-1.5,1.5])
    subplot(1,2,2)
    CAREA = [CAREA; s.area-param.area_target];
    plot(0:k-1,CAREA,'o-b','markerfacecolor','b','markersize',3);
    axis([-0.5,15,-0.06,0.1])
    set(gca,'fontsize',15)
    xlabel('iterations','interpreter','latex')
    ylabel('surface area constraint $A-A_{0}$','interpreter','latex')
    F(k) = getframe(gcf);
    writeVideo(vidfile,F(k));
end

close(vidfile)

%%
close all