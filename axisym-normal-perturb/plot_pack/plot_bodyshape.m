function plot_bodyshape(s,preset_num,color,panelends)
% this plots the axis-symmetric body
% 8/14/2020
% ruowen

persistent counter cmap

if nargin < 4
    panelends = 1;
end
if nargin < 3
    if isempty(counter)
        counter = 0;
    end
    if nargin < 2
    preset_num = 30;
    end
    if isempty(cmap)
        cmap = colormap(brewermap(preset_num, '*RdYlBu'));
        %cmap = [cmap, 0.5*ones(size(cmap,1),1)]; % set transparency in last column
    end
    
    if counter < preset_num
        counter = counter + 1;
    else
        counter = 1;
    end
    
    color = cmap(counter,:);    
end

% figure(1)
plot(real(s.x),imag(s.x),'-','color',color,'linewidth',1.5);
hold on
plot(-real(s.x),imag(s.x),'-','color',color,'linewidth',1.5);

if panelends
plot(real(s.x([1:s.p:end, end])),imag(s.x([1:s.p:end, end])),'*','color',color,'linewidth',1.5);
end

set(gca, 'fontsize', 20)
axis equal
grid on

% hold off

end