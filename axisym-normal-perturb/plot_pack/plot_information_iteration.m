% information for each iteration
clear
clear global

% load('./output/test_1215.mat') % prolate
% load('./output/test_1216_3.mat') % stomatocyte
% load('./output/test_1216_5.mat') % egg
% load('./output/test_1216_6.mat') %  wavy
load('./output/test_1218.mat') %  slender prolate

%%
% close all

% k=1;
% % k=67
k=length(iterations.Solver);
s = iterations.Solver(k);

figure(1), hold on, plot_bodyshape(s,[],[1,0,0])
% plot_bodyshape(s,[],[3,80,110]/255), title(['iteration ' num2str(k)],'interpreter','latex')
% axis([-1.5,1.5,-1.5,1.5])
axis([-1.5,1.5,-1.5,1.8])
figure, plot_uslip(s), title(['optimal slip for iteration ' num2str(k)],'interpreter','latex')

disp('k')
disp(k)

disp('volume')
disp(s.vol)

disp('surface area')
disp(s.area)

disp('U')
disp(s.U)

disp('eff')
disp(s.eff)

%%
for kk=1:length(iterations.Solver)
    C_vol(kk) = iterations.Solver(kk).C_vol;
    C_area(kk) = iterations.Solver(kk).C_area;
end

figure
plot(C_vol,'*-','linewidth',1.5)
xlim([0,length(iterations.Solver)+3])
set(gca,'fontsize',15), grid on
xlabel('iterations','interpreter','latex')
ylabel('volume constraint $V-V_{0}$','interpreter','latex')

figure
plot(C_area,'*-','linewidth',1.5)
xlim([0,length(iterations.Solver)+3])
set(gca,'fontsize',15), grid on
xlabel('iterations','interpreter','latex')
ylabel('surface area constraint $A-A_{0}$','interpreter','latex')
