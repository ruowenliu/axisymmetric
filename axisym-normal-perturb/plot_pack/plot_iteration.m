% plot iteration figures
clear
clear global
close all
for kk = 1

if kk == 1
load('./output/test_0103.mat') 
name = 'shape_from_prolate';
end
    
if kk == 2
load('./output/test_0104_capsule.mat') 
name = 'shape_from_capsule';
end

if kk == 3
load('./output/test_0105_oblate_2.mat') 
name = 'shape_from_oblate';
end

if kk == 4
load('./output/test_0106_mushroom.mat') 
name = 'shape_from_bighead';
end


figure, plot(iterations.Solver(end).arclen/iterations.Solver(end).ell, iterations.Solver(end).uslip, 'linewidth', 1.5)
hold on, plot(iterations.Solver(1).arclen/iterations.Solver(1).ell, iterations.Solver(1).uslip,':r', 'linewidth', 1.5)
set(gca,'fontsize',15)
xlabel('$s/\ell$','interpreter','latex')
ylabel('$u_s$','interpreter','latex')
figure, plot_bodyshape(iterations.Solver(1),[],[1,0,0])
hold on,plot_bodyshape(iterations.Solver(end),[],[0,0,1])
keyboard
%%
vidfile = VideoWriter(sprintf('%s_all.mp4',name),'MPEG-4');
vidfile.FrameRate = 2; % set speed
open(vidfile);

ITERnum = length(iterations.area);

EFF = zeros(ITERnum,1);
CVOL = zeros(ITERnum,1);
CAREA = zeros(ITERnum,1);
for k = 1:ITERnum
    EFF(k)=iterations.Solver(k).eff; 
    CVOL(k)=iterations.Solver(k).C_vol;
    CAREA(k)=iterations.Solver(k).C_area;
end

if kk==3
    EFF = EFF(1:end-4); CVOL = CVOL(1:end-4); CAREA = CAREA(1:end-4);
end

for k = 1:length(EFF)
% for k = length(EFF)
    close all
    figure
    subplot(2,2,1)
    plot_bodyshape(iterations.Solver(k),[],[0,102/255,204/255],0)    
    hold on
    plot(real(iterations.Solver(1).x),imag(iterations.Solver(1).x),':r','linewidth',1);
    plot(-real(iterations.Solver(1).x),imag(iterations.Solver(1).x),':r','linewidth',1);
    set(gca,'fontsize',15)
    title(['iter num = ' num2str(k-1)], 'interpreter','latex')
    axis([-1.8,1.8,-1.8,1.8])
    
    subplot(2,2,2)
    plot(0:k-1,EFF(1:k),'o-b','markerfacecolor','b','markersize',3);    
    axis([-0.5,ITERnum-0.5,min(EFF)-0.1*(max(EFF)-min(EFF)),max(EFF)+0.1*(max(EFF)-min(EFF))])
    grid on
    set(gca,'fontsize',15)
    xlabel('iterations','interpreter','latex')
    ylabel('efficiency $F_0 U^2 / J_W$','interpreter','latex')
    
    subplot(2,2,3)
    plot(0:k-1,CVOL(1:k),'o-b','markerfacecolor','b','markersize',3);
    axis([-0.5,ITERnum-0.5,min(CVOL)-0.1*(max(CVOL)-min(CVOL)),max(CVOL)+0.1*(max(CVOL)-min(CVOL))])
    grid on
    set(gca,'fontsize',15)
    xlabel('iterations','interpreter','latex')
    ylabel('volume constraint $V-V_{0}$','interpreter','latex')
    
    subplot(2,2,4)
    plot(0:k-1,CAREA(1:k),'o-b','markerfacecolor','b','markersize',3);
    axis([-0.5,ITERnum-0.5,min(CAREA)-0.1*(max(CAREA)-min(CAREA)),max(CAREA)+0.1*(max(CAREA)-min(CAREA))])
    grid on
    set(gca,'fontsize',15)
    xlabel('iterations','interpreter','latex')
    ylabel('surface area constraint $A-A_{0}$','interpreter','latex')
    
    F(k) = getframe(gcf);
    writeVideo(vidfile,F(k));

     figure
     hold on
     plot(iterations.Solver(1).arclen/iterations.Solver(1).ell, iterations.Solver(1).uslip,':r','linewidth',1.5)
     plot(iterations.Solver(k).arclen/iterations.Solver(k).ell, iterations.Solver(k).uslip,'linewidth',1.5)
     xlabel('$s/\ell$','interpreter','latex')
     ylabel('$u_s$','interpreter','latex')
     set(gca,'fontsize',15)
end

% close(vidfile)

% close all

end