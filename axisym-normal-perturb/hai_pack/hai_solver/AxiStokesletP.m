function G = AxiStokesletP(s,t)

X = [real(t.x);imag(t.x)]; Y = [real(s.x);imag(s.x)];
M = length(X)/2; N = length(Y)/2;

x1 = X(1:M); x2 = X(1+M:2*M);
y1 = Y(1:N); y2 = Y(1+N:2*N);  
G = zeros(M,2*N);  
for j =1:M
    Ker = AxiKernelP(y1, y2, x1(j), x2(j));   
    G(j, 1:N) = (Ker(:,1))';  
    G(j, N+1:end) = (Ker(:,2))';

end
W = ones(size(s.x))/4/pi;  
G = G.*[repmat(W', M, 1), repmat(W', M, 1)];
end
