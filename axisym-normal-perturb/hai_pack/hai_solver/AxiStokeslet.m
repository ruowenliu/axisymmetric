function G = AxiStokeslet(s,t)

X = [real(t.x);imag(t.x)]; Y = [real(s.x);imag(s.x)];
M = length(X)/2; N = length(Y)/2;

x1 = X(1:M); x2 = X(1+M:2*M);
y1 = Y(1:N); y2 = Y(1+N:2*N);  
G = zeros(2*M,2*N);  
for j =1:M
    Ker = AxiKernel(y1, y2, x1(j), x2(j));   
    G(j, 1:N) = ((Ker(:,2) + Ker(:,3)))';  
    G(j, N+1:end) = (Ker(:,4))';

    G(j+M, 1:N) = (Ker(:,5))';
    G(j+M, N+1:end) = ((Ker(:,1) + Ker(:,6)))';  
%     pause
end
W = 0.5*ones(size(s.x))/8/pi;  
G = G.*[repmat(W', 2*M, 1), repmat(W', 2*M, 1)];
G = 2*G;

end