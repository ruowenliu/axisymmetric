function for_plot()

close all

p=10; N = 10*p; qtype = 'p'; qntype = 'G'; s0.p = p;
alpha = 2; 
s0.Z = @(t) sin(t) + 1i*alpha*cos(t);
[s,~] = quadrp(s0, N, qtype, qntype); 
s = half_quadr(s);

[tt,ww] = weight_setup(8,2*8, 1e-01);

[v0, u0] = meshgrid(tt,s.t);
u = u0(:); v = v0(:);

s_xy = repmat(real(s.x),1,numel(tt)); s_z = repmat(imag(s.x),1,numel(tt));
S.x = s_xy(:).*cos(v); S.y = s_xy(:).*sin(v); S.z = s_z(:);

X = reshape(S.x,numel(s.t),numel(tt)); Y = reshape(S.y,numel(s.t),numel(tt)); Z = reshape(S.z,numel(s.t),numel(tt)); 

figure(1),clf,h=surf(X,Y,Z);axis equal; colormap(jet(256));

keyboard

end