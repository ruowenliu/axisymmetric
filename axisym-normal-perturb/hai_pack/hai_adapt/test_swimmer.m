% with axi traction kernel 
%
% Hai 04/24/20

clear all
Shape = 'sphere';
% Shape = 'ellipseZ';

% axi symmetric setup
p = 10; % axi symmetric solver panel order
N = 2*30*p; qtype = 'p'; qntype = 'G'; s0.p = p;
switch Shape
    case 'sphere'
        alpha = 1; 
    case 'ellipseZ'
        alpha = 5;
end
s0.Z = @(t) 1/alpha*sin(t) - 1i*cos(t); 

% 2 mesh size to test interpolation of density
s1 = quadrp(s0, N, qtype); 
s1 = half_quadr(s1); 
s2 = quadrp(s0, N+10*p, qtype); 
s2 = half_quadr(s2);

beta = 0; 
%% solve for 1st mesh
slip1 = 1.5*(sin(s1.t)+beta*sin(2*(s1.t)));
f1 = [slip1;slip1].*[real(s1.tang);imag(s1.tang)];
alpSLP1 = AlpertSphereSLPMat(s1);
Mat1 = [alpSLP1 [zeros(numel(s1.x),1);-ones(numel(s1.x),1)];...
       zeros(1,numel(s1.x)) (real(s1.x).*s1.ws(:))' 0];
soln1 =  Mat1\[f1;0]; 
tau1 =  soln1(1:end-1); % tau1_xy = tau1(1:end/2); tau1_z = tau1(end/2+1:end); 
T1 = AlpertSphereSLPMatT(s1);
trac1 = (-eye(size(T1))/2 + T1)*tau1; trac1_xy = trac1(1:end/2); trac1_z = trac1(end/2+1:end);
% figure(),plot(s1.t,tau1(end/2+1:end),'.-r'); hold on, plot(s1.t,trac1_z,'.-b')
fprintf(' \n');
fprintf('Check total force (maybe missing a factor if non-zero) with n1 = %.3g ... \n',numel(s1.x));
fprintf('  1. z-axis by integrating density function = %.3g\n', sum(tau1(end/2+1:end).*s1.ws.*real(s1.x)));
fprintf('  2. z-axis by integrating traction = %.3g\n', sum(trac1_z.*s1.ws.*real(s1.x)));

%% solve for 2nd mesh
slip2 = 1.5*(sin(s2.t)+beta*sin(2*(s2.t)));
f2 = [slip2;slip2].*[real(s2.tang);imag(s2.tang)];
alpSLP2 = AlpertSphereSLPMat(s2);
Mat2 = [alpSLP2 [zeros(numel(s2.x),1);-ones(numel(s2.x),1)];...
       zeros(1,numel(s2.x)) (real(s2.x).*s2.ws(:))' 0];
soln2 =  Mat2\[f2;0]; 
tau2 =  soln2(1:end-1); % tau2_xy = tau2(1:end/2); tau2_z = tau2(end/2+1:end); 
T2 = AlpertSphereSLPMatT(s2);
trac2 = (-eye(size(T2))/2 + T2)*tau2; trac2_xy = trac2(1:end/2); trac2_z = trac2(end/2+1:end);
% figure(),plot(s2.t,tau2(end/2+1:end),'.-r'); hold on, plot(s2.t,trac2_z,'.-b')
fprintf(' \n');
fprintf('Check total force (maybe missing a factor if non-zero) with n2 = %.3g ... \n',numel(s2.x));
fprintf('  1. z-axis by integrating density function = %.3g\n', sum(tau2(end/2+1:end).*s2.ws.*real(s2.x)));
fprintf('  2. z-axis by integrating traction = %.3g\n', sum(trac2_z.*s2.ws.*real(s2.x)));

fprintf(' \n');
fprintf('Check power related between two mesh n1 = %.3g and n2 = %.3g ... \n',numel(s1.x),numel(s2.x));
fprintf('  1. self comparison error along z-axis = %.3g\n', abs(sum(trac2_z.*s2.ws.*real(s2.x).*(f2(end/2+1:end)+soln1(end)))-sum(trac1_z.*s1.ws.*real(s1.x).*(f1(end/2+1:end)+soln2(end)))));
% sum(trac2_z.*s2.ws.*real(s2.x).*(f2(end/2+1:end)+soln1(end)))
% J = (real(s.x).*s.ws(:))'*(f(1:end/2).*real(s.tang).*slip + f(end/2+1:end).*(imag(s.tang).*slip+U))*2*pi;
J = (real(s2.x).*s2.ws(:))'*(trac2_xy.*real(s2.tang).*slip2 + trac2_z.*(imag(s2.tang).*slip2+soln2(end)))*2*pi;


fprintf(' \n');
fprintf('Check centroid velocity related between two mesh n1 = %.3g and n2 = %.3g ... \n',numel(s1.x),numel(s2.x));
fprintf('  1. self comparison error along z-axis = %.3g\n', abs(soln1(end)-soln2(end)));
soln1(end)

% downsampling from s2 to s1
[stdt, ~] = gauss(p);
stdbw2 = baryweights(stdt); stdbw2 = repmat(stdbw2/s2.np,1,s2.np); stdbw2 = stdbw2(:);
Lp2_down = baryprojs(s2.t-pi/2, stdbw2,s1.t-pi/2);
Trac2_xy = Lp2_down*trac2_xy; Trac2_z = Lp2_down*trac2_z;
fprintf(' \n');
fprintf('Check traction between two mesh n1 = %.3g and n2 = %.3g ... \n',numel(s1.x),numel(s2.x));
fprintf('  1. max pointwise error along z-axis = %.3g\n', max(abs(Trac2_z -trac1_z)));
fprintf('  2. max pointwise error along x-axis = %.3g\n', max(abs(Trac2_xy -trac1_xy)));


keyboard