% Problem: 
% maximize the EFFICIENCY of micro-swimmer
% subject to surface area and volume constraints

% 10/02/2020
% Ruowen Liu, ruowen@umich.edu, ruowenliu2010@gmail.com

%% 0. preset
close all
clear
clear global
clear geometryBSP_prd
clear uslipBSP_prd
clear plot_bodyshape
format long
format compact

tic
%% 1. set Augmented Lagrangian Method parameters
sig = 10;
lam = [0,0];

%% 2. set current geometry parameter vector geom_xi

param.p = 10; % p is the number of points in one panel
param.panel_num = 30*2; % the first number is the default num of panels for the generating arc (top to bottom)
param.N = param.panel_num*param.p;
param.xtop = 0; param.xbot = 0;  % must equal zero

param.shapeDoF = 11;  % This number is M+1 (all grid points)
r1 = 0.8;
r2 = 1;
x = r1*cos(pi/2-linspace(0,pi,param.shapeDoF))';
z = r2*sin(pi/2-linspace(0,pi,param.shapeDoF))';
xi_geom = [x(2:end-1);z(1:end)]; % length: shapeNc*2-2
rng(1)
xi_geom = xi_geom + 0.04*randn(size(xi_geom));

% xi_geom_old = xi_geom;

% xi_geom_x = xi_geom(1:param.shapeDoF-2);
% xi_geom_z = xi_geom(param.shapeDoF-1:end);

% x_basis = zeros(param.N/2,param.shapeDoF-2);
% for k = 1:param.shapeDoF-2
% xi_geom_x = zeros(size(xi_geom_x)); xi_geom_x(k) = 1;
% xi_geom_z = zeros(size(xi_geom_z));
% xi_geom = [xi_geom_x;xi_geom_z];
% [s, ~, ~] = vec_to_quadrp_full(xi_geom, param);
% s = arclen(s);
% 
% x_basis(:,k) = real(s.x);
% % figure(1), plot_bodyshape(s,[],[1,0,0]), title('test (initial) shape')
% subplot(3,3,k), plot(s.t , real(s.x), 'linewidth',1.5), title(['$x_k(t)$ for $k=$ ' num2str(k) ' ($M_{\Gamma} = 11$)'],'interpreter','latex'), grid on
% set(gca,'fontsize',15)
% axis([-0.1,pi+0.1,-0.5, 1.5])
% % figure(3), plot(s.t , imag(s.x), 'linewidth',1.5)
% end
% 
% [s_old, ~, ~] = vec_to_quadrp_full(xi_geom_old, param);
% s_old = arclen(s_old);
% max(abs(real(s_old.x) - x_basis*xi_geom_old(1:param.shapeDoF-2)))


% z_basis = zeros(param.N/2,param.shapeDoF);
% for k = 1:param.shapeDoF
% xi_geom_x = zeros(size(xi_geom_x)); 
% xi_geom_z = zeros(size(xi_geom_z)); xi_geom_z(k) = 1;
% xi_geom = [xi_geom_x;xi_geom_z];
% [s, ~, ~] = vec_to_quadrp_full(xi_geom, param);
% s = arclen(s);
% 
% z_basis(:,k) = imag(s.x);
% % figure(1), plot_bodyshape(s,[],[1,0,0]), title('test (initial) shape')
% subplot(4,3,k), plot(s.t , imag(s.x), 'linewidth',1.5), title(['$z_k(t)$ for $k=$ ' num2str(k) ' ($M_{\Gamma} = 11$)'],'interpreter','latex'), grid on
% set(gca,'fontsize',15)
% axis([-0.1,pi+0.1,-0.5, 1.5])
% end
% [s_old, ~, ~] = vec_to_quadrp_full(xi_geom_old, param);
% s_old = arclen(s_old);
% max(abs(imag(s_old.x) - z_basis*xi_geom_old(param.shapeDoF-1:end)))


% %%%%%%

[s, ~, ~] = vec_to_quadrp_full(xi_geom, param);
s = arclen(s);
figure(1), plot_bodyshape(s,[],[1,0,0]), title('test (initial) shape')
% figure(2), plot(s.t , real(s.x))
% figure(3), plot(s.t , imag(s.x))

%% 3. compute F-vector and F0
param.slipDoF = 16; % degree of freedom for uslip

us_basis = zeros(param.N/2,param.slipDoF);
s_mode = cell(param.slipDoF,1);
for mode = 1:param.slipDoF
    controlpoint_half = zeros(param.slipDoF,1); controlpoint_half(mode) = 1;
    controlpoint = [0;controlpoint_half;0; -flip(controlpoint_half);0];
    usfunc_eachmode = uslipBSP_prd(controlpoint);
    s_eachmode = s;
    s_eachmode = arclen(s_eachmode);
    s_eachmode.uslip = usfunc_eachmode(s.t);   
    us_basis(:,mode) = s_eachmode.uslip;
    s_eachmode = forwardsolve_nnf(s_eachmode,'adjoint','basis');
    s_mode{mode, 1} = s_eachmode;
    subplot(4,4,mode), plot(s_eachmode.arclen/s_eachmode.ell, us_basis(:,mode), 'linewidth',1.5), title(['$U_k(\alpha)$ for $k=$ ' num2str(mode) ' ($M_{u} = 16$)'],'interpreter','latex'), grid on
    set(gca,'fontsize',15)
    axis([-0.1,1+0.1,-0.5, 1.5])
end


%% 4. compute B matrix and solve for max_eff and slip_xi
Bmatrix = zeros(param.slipDoF,param.slipDoF);
Fvec = zeros(param.slipDoF,1);
for p = 1:param.slipDoF
    for q = 1:param.slipDoF
        Bmatrix(p,q) = 2*pi*s.ws'*(dotv(s_mode{p,1}.trac,(s_mode{q,1}.uslip.*s.tang)).*real(s.x)) - s_mode{p,1}.Fk*s_mode{q,1}.Fk/s_mode{mode,1}.F0;
    end
    Fvec(p) = s_mode{p,1}.Fk;
end
xi_slip = Bmatrix\Fvec;
max_eigenvalue = Fvec'*xi_slip;

% eff_new = Fvec'*xi_slip/s.F0;

controlpoint = [0;xi_slip;0; -flip(xi_slip);0];
usfunc = uslipBSP_prd(controlpoint);
s.uslip = usfunc(s.t);    

%
uslip2 = us_basis*xi_slip;
max(abs(s.uslip-uslip2))
%

figure, plot(s.arclen/s.ell, s.uslip), title('U^s (s/l)')
s = forwardsolve_nnf(s,'adjoint');

% [EigenV,EigenD] = eig(Fvec*Fvec',Bmatrix);

toc
% %% 5. perturb the shape - xi_geom
% 
% % compute eigenvalue_derivative
% 
